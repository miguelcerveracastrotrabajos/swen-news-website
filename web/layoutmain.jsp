<%@page import="java.util.List"%>
<%@page import="Models.ModelUser"%>
<%@page import="Models.ModelArticle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>SWEN</title>
    
    
    <!-- Favicon -->
    <link rel="icon" href="/ProjetoArtigos/img/icon.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="css/maincss.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/mainjavascript.js"></script>
    
    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <% ModelUser auth = (ModelUser)request.getAttribute("auth"); %>
        <!-- Navbar Area -->
        <div class="mag-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="magNav">

                    <!-- Nav brand -->
                    <a href="index.html" class="nav-brand"><img src="img/core-img/logo.png" alt=""></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Nav Content -->
                    <div class="nav-content d-flex align-items-center">
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <% if(auth==null){%><li><a href="/ProjetoArtigos/main_login">Login</a></li><% } else { %> <li><a href="/ProjetoArtigos/login_user">Logout</a></li> <% } %>
                                    <li><a href="/ProjetoArtigos/main_other">Other Headlines</a></li>
                                    <li><a href="/ProjetoArtigos/main_aboutus">About Us</a></li>
                                    <li><a href="index"><img src="/ProjetoArtigos/img/bo_extended.png" style="height:62px;"></a></li>
                                    <% if(auth!=null){%>
                                    <li id="autenticateSquare" class="navbar-text"><span style="margin-right:20px;"><%= auth.getUserName() %></span><img src="img/<%=auth.getMedia().getMediaId()+"."+auth.getMedia().getMediaType() %>"></li>
                                    <% } %>
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div>

                        <div class="top-meta-data d-flex align-items-center">
                            <!-- Top Search Area -->
                            <!-- <div class="top-search-area">
                                <form action="index.html" method="post">
                                    <input type="search" name="top-search" id="topSearch" placeholder="Search and hit enter...">
                                    <button type="submit" class="btn"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div> -->
                            <!-- Login -->
                            <a href="/ProjetoArtigos/" class="login-btn"><img style="height:75px" src="/ProjetoArtigos/img/icon.png"></a>
                            <!-- Submit Video -->
                            <!-- <a href="submit-video.html" class="submit-video"><span><i class="fa fa-cloud-upload"></i></span> <span class="video-text">Submit Video</span></a> -->
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->
</head>
<body>
    <style>
        ::-webkit-scrollbar { 
            display: none; 
        }
    </style>
    <div id="container" style="padding-top:15px;width:100%;height:100%;overflow: hidden;">
        <jsp:include page="views/${contentPage}.jsp" />
    </div>
</body>
<!-- ##### Footer Area Start ##### -->
<footer class="footer-area">
    <div class="container">
        <div class="row">
            <!-- Footer Widget Area -->
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="footer-widget">
                    <!-- Logo -->
                    <a href="/ProjetoArtigos/main_about" class="login-btn"><img class="icon-brand-now" src="/ProjetoArtigos/img/icon.png"></a>
                    <p>Swen is the news site made particulary for you! Premium users that want reliable and trusworthy information.</p>
                    <div class="footer-social-info">
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>

            <!-- Footer Widget Area -->
            <div class="col-12 col-sm-6 col-lg-3">

            </div>

            <!-- Footer Widget Area -->
            <div class="col-12 col-sm-6 col-lg-3">

            </div>

            <!-- Footer Widget Area -->
            <div class="col-12 col-sm-6 col-lg-3">
              <div class="footer-widget">
                  <h6 class="widget-title">All Videos</h6>
                  <!-- Single Blog Post -->
                   <%
                    ModelArticle u = new ModelArticle();

                    List<ModelArticle> lstactive = u.listActive(); 
                    if(lstactive!=null) {
                            for (ModelArticle article : lstactive) {
                                if(new String(article.getImage().getMediaType()).equals("mp4")){

                    %>
                    <div class="single-blog-post style-2 d-flex">
                        <div class="post-thumbnail">
                            <video style="width:100%;">
                                <source src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" type="video/<%= article.getImage().getMediaType() %>">
                             </video>
                        </div>
                        <div class="post-content">
                            <a href="/ProjetoArtigos/articles?id=<%= article.getArticleId()%>" class="post-title"><%= article.getArticleTitle() %></a>
                            <div class="post-meta d-flex justify-content-between">
                                <a href="#"><i class="fa fa-eye" aria-hidden="true"></i>  <%= article.getArticleViews() %></a>

                            </div>
                        </div>
                    </div>
                     <%
                            }
                        } 
                    }
                    %>
              </div>
            </div>
        </div>
    </div>

    <!-- Copywrite Area -->
    <div class="copywrite-area">
        <div class="container">
            <div class="row">
                <!-- Copywrite Text -->
                <div class="col-12 col-sm-6" style="color:white;">
                    <div><strong>Time After Load</strong></div>
                    <div id="gettingValuesExample">
                        <span class="days">0</span><span> days</span>
                        <span class="hours">0</span><span> hours</span>
                        <span class="minutes">0</span><span> minutes</span>
                        <span class="seconds">0</span><span> seconds</span>
                        <span class="secondTenths">0</span><span> tenth of seconds</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- ##### Footer Area End ##### -->

</html>