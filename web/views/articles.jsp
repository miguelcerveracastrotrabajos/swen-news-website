<%@page import="java.util.List"%>
<%@page import="Models.ModelArticle"%>
<%@page import="Models.ModelUser"%>
<%@page import="Models.ModelMedia"%>
<h1>Articles</h1>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-active-tab" data-toggle="pill" href="#pills-active" role="tab" aria-controls="pills-active" aria-selected="true">Active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-inactive-tab" data-toggle="pill" href="#pills-inactive" role="tab" aria-controls="pills-inactive" aria-selected="false">Inactive</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active table-responsive" id="pills-active" role="tabpanel" aria-labelledby="pills-active-tab">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Image</th>
                <th scope="col">Title</th>
                <th scope="col">SubTitle</th>
                <th scope="col">Owner</th>
                <th scope="col">Timestamps</th>
                <th scope="col">Options</th>
              </tr>
            </thead>
            <tbody>
              <%
                  ModelArticle a = new ModelArticle();
                  List<ModelArticle> lstactive = (List<ModelArticle>)request.getAttribute("lstactive");
                  if(lstactive!=null) {
                      for (ModelArticle article : lstactive) {
              %>
              <tr>
                <td>
                    <% if(new String(article.getImage().getMediaType()).equals("mp4")){ %> 
                        <video width="150" height="84.313">
                            <source src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" type="video/<%= article.getImage().getMediaType() %>">
                         </video>
                    <% } else {%>
                        <img src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" class="imageMiniArticle">
                    <% } %>
                </td>
                <td><%= article.getArticleTitle()  %></td>
                <td><%= article.getArticleSubtitle() %></td>
                <td><%= article.getAuthor().getUserName() %></td>
                <td class="timestampFont">    
                    <div>CA: <%= article.getCreated_at()%></div>
                    <div>UA: <%= article.getUpdated_at()%></div>
                </td>
                <td>
                    <a href="/ProjetoArtigos/articles?id=<%= article.getArticleId()%>" class="btn btn-success">
                        <i class="fas fa-eye"></i>
                    </a>
                </td>
              </tr>
              <%
                      }
                  }
              %>
            </tbody>
        </table>
    </div>
    <div class="tab-pane fade table-responsive" id="pills-inactive" role="tabpanel" aria-labelledby="pills-inactive-tab">
         <table class="table">
            <thead>
              <tr>
                <th scope="col">Image</th>
                <th scope="col">Title</th>
                <th scope="col">SubTitle</th>
                <th scope="col">Owner</th>
                <th scope="col">Timestamps</th>
                <th scope="col">Options</th>
              </tr>
            </thead>
            <tbody>
              <%
                  List<ModelArticle> lstinactive = (List<ModelArticle>)request.getAttribute("lstinactive");
                  if(lstinactive!=null) {
                      for (ModelArticle article : lstinactive) {
              %>
                <form onsubmit="event.preventDefault(); activateArticle(new FormData($(this)[0]))">
                    <tr>
                        <input type="hidden" name="articleid" value="<%= article.getArticleId()%>">
                        <td><img src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" class="imageMiniArticle"></td>
                        <td><%= article.getArticleTitle()  %></td>
                        <td><%= article.getArticleSubtitle() %></td>
                        <td><%= article.getAuthor().getUserName() %></td>
                        <td class="timestampFont">    
                            <div>CA: <%= article.getCreated_at()%></div>
                            <div>UA: <%= article.getUpdated_at()%></div>
                        </td>
                        <td>
                            <button type="submit" class="btn btn-outline-success">
                                <i class="fas fa-check"></i>
                            </button>
                        </td>
                    </tr>
                </form>
             
              <%
                      }
                  }
              %>
            </tbody>
        </table>
    </div>
</div>