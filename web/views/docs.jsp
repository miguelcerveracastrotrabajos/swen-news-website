<h1>Docs</h1>
<h2>Index</h2>

<h2>#Routes</h2>

<h3>Method POST</h3>
<table class="table">
    <tr>
        <th>
            URL
        </th>
        <th>
            Designation
        </th>
    </tr>
    <tr>
        <td class="timestampFont">
            /articles_activate
        </td>
        <td>
            Activate a deleted article
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /types_activate
        </td>
        <td>
            Activate a deleted type user
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /users_activate
        </td>
        <td>
            Activate a deleted user
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /articles?id={id} 
        </td>
        <td>
            Edit or Delete Article with certain id
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /users_authenticate
        </td>
        <td>
            Login after submit
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /articles_create
        </td>
        <td>
            Create an Article
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /types_create
        </td>
        <td>
            Create an User Type
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /users_create
        </td>
        <td>
            Create an User
        </td>
    </tr>
     <tr>
        <td class="timestampFont">
            /types?id={id} 
        </td>
        <td>
            Edit or Delete User Type with certain id
        </td>
    </tr>
</table>
<h3>Method GET</h3>
<table class="table">
    <tr>
        <th>
            URL
        </th>
        <th>
            Designation
        </th>
    </tr>
    <tr>
        <td class="timestampFont">
            /articles?id={id} 
        </td>
        <td>
            View of an Article of certain id
        </td>
    </tr>
     <tr>
        <td class="timestampFont">
            /articles
        </td>
        <td>
            View of all articles
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /users_authenticate
        </td>
        <td>
            Logout authenticated user
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /articles_create
        </td>
        <td>
            View of Create an Article
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /types_create
        </td>
        <td>
            View of Create an User Type
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /users_create
        </td>
        <td>
            View of Create an User
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /dashboard
        </td>
        <td>
            View of Dashboard
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /docs
        </td>
        <td>
            View of Documentation
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /index
        </td>
        <td>
            View of Login
        </td>
    </tr>
     <tr>
        <td class="timestampFont">
            /logs
        </td>
        <td>
            View of Logs
        </td>
    </tr>
    <tr>
        <td class="timestampFont">
            /types
        </td>
        <td>
            View of all types
        </td>
    </tr>
   
</table>
<h2>#Database</h2>

<h3>#Database Tables</h3>

<h3>#Database Storage Procedures</h3>

<h3>#Database Triggers</h3>