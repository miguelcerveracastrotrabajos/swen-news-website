 
<%@page import="java.util.Collections"%>
<%@page import="java.util.List"%>
<%@page import="Models.ModelArticle"%>
<%@page import="Models.ModelMedia"%>
<!-- ##### Hero Area Start ##### -->
    <% 
        ModelArticle u = new ModelArticle();

        List<ModelArticle> lstactive = u.listActive(); 
        
    %>
    
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Mag Posts Area Start ##### -->
    <section class="mag-posts-area d-flex flex-wrap">

        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Left Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="post-sidebar-area left-sidebar mt-30 mb-30 bg-white box-shadow">
            <!-- Sidebar Widget -->
            <div class="single-sidebar-widget p-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>Most Popular</h5>
                </div>
                <div >
                    <%  
                       
                        if(lstactive!=null) {
                            List<ModelArticle> newlstactive =lstactive;
                            Collections.sort(newlstactive);
                            for (ModelArticle article : newlstactive) {   

                    %>
                    <!-- Single Blog Post -->
                    <div class="single-blog-post d-flex">
                        <div class="post-thumbnail">
                            <% if(new String(article.getImage().getMediaType()).equals("mp4")){ %> 
                                <video style="width:100%;">
                                    <source src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" type="video/<%= article.getImage().getMediaType() %>">
                                 </video>
                            <% } else {%>
                                <img src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>">
                            <% } %>
                        </div>
                        <div class="post-content">
                            <a href="/ProjetoArtigos/main_article?id=<%= article.getArticleId()%>" class="post-title"> <%= article.getArticleTitle() %> </a>
                            <span class="mini-tag">
                                <i class="fa fa-eye" aria-hidden="true"></i><%= article.getArticleViews() %>
                            </span>
                        </div>
                    </div>
                    <%
                            }
                        }
                    %>
                </div>
            </div>
        </div>

        <!-- >>>>>>>>>>>>>>>>>>>>
             Main Posts Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="mag-posts-content mt-30 mb-30 p-30 box-shadow">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <%
                  int index = 0;
                  if(lstactive!=null) {
                    for (ModelArticle article : lstactive) {    
                        if(new String(article.getImage().getMediaType()).equals("jpg")){
                            index++;
                  %>
                    <li data-target="#carouselExampleIndicators" data-slide-to="<%=index%>" class="<% if(new Integer(index).equals(1)){%> active <% } %>"></li>
                  <%
                        }
                    }
                  }    
                  %>
                </ol>
                <div class="carousel-inner">
                  <%
                  index=0;
                  if(lstactive!=null) {
                    for (ModelArticle article : lstactive) {    
                        if(new String(article.getImage().getMediaType()).equals("jpg")){
                            index++;
                  %>
                  <div class="carousel-item <% if(new Integer(index).equals(1)){%> active <% } %>">
                    <img class="d-block w-100" src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" alt="<%= article.getArticleTitle() %>" style="height:550px;">
                      <div class="carousel-caption d-none d-md-block">
                        <h5 style="color:white"><%= article.getArticleTitle() %></h5>
                      </div>
                  </div>
                  <%
                        }
                    }
                  }    
                  %>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
            </div>


            <!-- Feature Video Posts Area -->
            <div class="feature-video-posts mb-30">

            </div>

            <!-- Most Viewed Videos -->
            <div class="most-viewed-videos mb-30">

            </div>

            <!-- Sports Videos -->
            <div class="sports-videos-area">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 >All News</h5>
                </div>

                <div class="sports-videos-slides owl-carousel mb-30">
                    <!-- Single Featured Post -->
                    <div class="single-featured-post">
                        <!-- Thumbnail -->
                        <div class="post-thumbnail mb-50">
                            <img src="img/bg-img/22.jpg" alt="">
                            <a href="video-post.html" class="video-play"><i class="fa fa-play"></i></a>
                        </div>
                        <!-- Post Contetnt -->
                        <div class="post-content">
                            <div class="post-meta">
                                <a href="#">MAY 8, 2018</a>
                                <a href="archive.html">lifestyle</a>
                            </div>
                            <a href="video-post.html" class="post-title">A Closer Look At Our Front Porch Items From Lowe?s</a>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                        </div>
                        <!-- Post Share Area -->
                        <div class="post-share-area d-flex align-items-center justify-content-between">
                            <!-- Post Meta -->
                            <div class="post-meta pl-3">
                                <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 1034</a>
                            </div>
                            <!-- Share Info -->
                            <div class="share-info">
                                <a href="#" class="sharebtn"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <!-- All Share Buttons -->
                                <div class="all-share-btn d-flex">
                                    <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#" class="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                    <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Featured Post -->
                    <div class="single-featured-post">
                        <!-- Thumbnail -->
                        <div class="post-thumbnail mb-50">
                            <img src="img/bg-img/22.jpg" alt="">
                            <a href="video-post.html" class="video-play"><i class="fa fa-play"></i></a>
                        </div>
                        <!-- Post Contetnt -->
                        <div class="post-content">
                            <div class="post-meta">
                                <a href="#">MAY 8, 2018</a>
                                <a href="archive.html">lifestyle</a>
                            </div>
                            <a href="video-post.html" class="post-title">A Closer Look At Our Front Porch Items From Lowe?s</a>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                        </div>
                        <!-- Post Share Area -->
                        <div class="post-share-area d-flex align-items-center justify-content-between">
                            <!-- Post Meta -->
                            <div class="post-meta pl-3">
                                <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 1034</a>

                            </div>
                            <!-- Share Info -->
                            <div class="share-info">
                                <a href="#" class="sharebtn"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <!-- All Share Buttons -->
                                <div class="all-share-btn d-flex">
                                    <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#" class="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                    <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Featured Post -->
                    <div class="single-featured-post">
                        <!-- Thumbnail -->
                        <div class="post-thumbnail mb-50">
                            <img src="img/bg-img/22.jpg" alt="">
                            <a href="video-post.html" class="video-play"><i class="fa fa-play"></i></a>
                        </div>
                        <!-- Post Contetnt -->
                        <div class="post-content">
                            <div class="post-meta">
                                <a href="#">MAY 8, 2018</a>
                                <a href="archive.html">lifestyle</a>
                            </div>
                            <a href="video-post.html" class="post-title">A Closer Look At Our Front Porch Items From Lowe?s</a>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                        </div>
                        <!-- Post Share Area -->
                        <div class="post-share-area d-flex align-items-center justify-content-between">
                            <!-- Post Meta -->
                            <div class="post-meta pl-3">
                                <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 1034</a>

                            </div>
                            <!-- Share Info -->
                            <div class="share-info">
                                <a href="#" class="sharebtn"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                <!-- All Share Buttons -->
                                <div class="all-share-btn d-flex">
                                    <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#" class="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                    <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <%
                    index = 0;
                    if(lstactive!=null) {
                            for (ModelArticle article : lstactive) {
                                if(index<6){
                    %>
                    <!-- Single Blog Post -->
                    <div class="col-12 col-lg-6">
                        <div class="single-blog-post d-flex style-3 mb-30">
                            <div class="post-thumbnail">
                                <% if(new String(article.getImage().getMediaType()).equals("mp4")){ %> 
                                    <video style="width:100%;">
                                        <source src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" type="video/<%= article.getImage().getMediaType() %>">
                                     </video>
                                <% } else {%>
                                    <img src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>">
                                <% } %>
                            </div>
                            <div class="post-content">
                                <a href="/ProjetoArtigos/main_article?id=<%= article.getArticleId()%>" class="post-title"> <%= article.getArticleTitle() %> </a>
                                <div class="post-meta d-flex">
                                    <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> <%= article.getArticleViews() %></a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <%
                            }
                        index++;
                        } 
                    }
                    %>
                </div>
            </div>
        </div>
        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Right Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="post-sidebar-area right-sidebar mt-30 mb-30 box-shadow bg-white">
                <!-- Sidebar Widget -->
                <div class="single-sidebar-widget p-30">
                    <!-- Social Followers Info -->
                    <div class="social-followers-info">
                        <!-- Facebook -->
                        <a href="#" class="facebook-fans"><i class="fa fa-facebook"></i> <span class="jumbo" id="FacebookCounter">4,360</span> <span>Fans</span></a>
                        <!-- Twitter -->
                        <a href="#" class="twitter-followers"><i class="fa fa-twitter"></i> <span class="counter">3,280</span> <span>Followers</span></a>
                        <!-- YouTube -->
                        <a href="#" class="youtube-subscribers"><i class="fa fa-youtube"></i> <span class="counter">1250</span> <span>Subscribers</span></a>
                        <!-- Google -->
                        <a href="#" class="google-followers"><i class="fa fa-google-plus"></i> <span class="counter">4,230</span> <span>Followers</span></a>
                    </div>
                </div>
    
                
    
                <!-- Sidebar Widget -->
                <div class="single-sidebar-widget p-30">
                    <!-- Section Title -->
                    <div class="section-heading">
                        <h5>About Us</h5>
                    </div>
    
                    <div class="newsletter-form">
                        <p>Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                
                        </p>
                    </div>
                     <div class="button-back-article"><a href="/ProjetoArtigos/main_aboutus" class="btn mag-btn w-100" style="    margin-left: 25%;">See More</a></div>
                </div>
            </div>
        
    </section>
    <!-- ##### Mag Posts Area End ##### -->
    <script>
        $('.carousel').carousel()
    </script>