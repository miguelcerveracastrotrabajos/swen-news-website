 
<%@page import="java.util.Collections"%>
<%@page import="java.util.List"%>
<%@page import="Models.ModelArticle"%>
<%@page import="Models.ModelMedia"%>
<!-- ##### Hero Area Start ##### -->
    <% 
        ModelArticle u = new ModelArticle();

        List<ModelArticle> lstactive = u.listActive(); 
        ModelArticle article = (ModelArticle)request.getAttribute("article");
    %>
    
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Mag Posts Area Start ##### -->
    <section class="mag-posts-area d-flex flex-wrap">

        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Left Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="post-sidebar-area left-sidebar mt-30 mb-30 bg-white box-shadow">
            <!-- Sidebar Widget -->
            <div class="single-sidebar-widget p-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>Most Popular</h5>
                </div>
                <div >
                    <%  
                       
                        if(lstactive!=null) {
                            List<ModelArticle> newlstactive =lstactive;
                            Collections.sort(newlstactive);
                            for (ModelArticle articleU : newlstactive) {     

                    %>
                    <!-- Single Blog Post -->
                    <div class="single-blog-post d-flex">
                        <div class="post-thumbnail">
                            <% if(new String(articleU.getImage().getMediaType()).equals("mp4")){ %> 
                                <video style="width:100%;">
                                    <source src="img/<%=articleU.getImage().getMediaId()+"."+articleU.getImage().getMediaType() %>" type="video/<%= articleU.getImage().getMediaType() %>">
                                 </video>
                            <% } else {%>
                                <img src="img/<%=articleU.getImage().getMediaId()+"."+articleU.getImage().getMediaType() %>">
                            <% } %>
                        </div>
                        <div class="post-content">
                            <a href="/ProjetoArtigos/main_article?id=<%= articleU.getArticleId()%>" class="post-title"> <%= articleU.getArticleTitle() %> </a>
                            <span class="mini-tag">
                                <i class="fa fa-eye" aria-hidden="true"></i><%= articleU.getArticleViews() %>
                            </span>
                        </div>
                    </div>
                    <%
                            }
                        }
                    %>
                </div>
            </div>
        </div>

        <!-- >>>>>>>>>>>>>>>>>>>>
             Main Posts Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="mag-posts-content mt-30 mb-30 p-30 box-shadow">
            <div class="blog-thumb mb-30">
                <% if(new String(article.getImage().getMediaType()).equals("mp4")){ %> 
                    <video width="100%" controls>
                        <source src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" type="video/<%= article.getImage().getMediaType() %>">
                     </video>
                <% } else {%>
                    <img src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" style="width:100%;max-height:624.5px;">
                <% } %>
            </div>
            <div class="blog-content">
                <div class="post-meta">
                    <a href="#"><%= article.getUpdated_at() %></a>
                </div>
                <h4 class="post-title"><%= article.getArticleTitle() %></h4>
                <!-- Post Meta -->
                <div class="post-meta-2">
                    <h6><%= article.getArticleSubtitle()%></h6>
                </div>
                <p><%= article.getArticleBody()%></p>
                <div class="button-back-article"><a href="/ProjetoArtigos/main" class="btn mag-btn w-100">Go Back</a></div>
            </div>
        </div>
        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Right Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="post-sidebar-area right-sidebar mt-30 mb-30 box-shadow bg-white">
                <!-- Sidebar Widget -->
                <div class="single-sidebar-widget p-30">
                    <!-- Social Followers Info -->
                    <div class="social-followers-info">
                        <!-- Facebook -->
                        <a href="#" class="facebook-fans"><i class="fa fa-facebook"></i> 4,360 <span>Fans</span></a>
                        <!-- Twitter -->
                        <a href="#" class="twitter-followers"><i class="fa fa-twitter"></i> 3,280 <span>Followers</span></a>
                        <!-- YouTube -->
                        <a href="#" class="youtube-subscribers"><i class="fa fa-youtube"></i> 1250 <span>Subscribers</span></a>
                        <!-- Google -->
                        <a href="#" class="google-followers"><i class="fa fa-google-plus"></i> 4,230 <span>Followers</span></a>
                    </div>
                </div>
    
                
    
                <!-- Sidebar Widget -->
                <div class="single-sidebar-widget p-30">
                    <!-- Section Title -->
                    <div class="section-heading">
                        <h5>About Us</h5>
                    </div>
    
                    <div class="newsletter-form">
                        <p>Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                
                        </p>
                    </div>
                     <div class="button-back-article"><a href="/ProjetoArtigos/main_aboutus" class="btn mag-btn w-100" style="    margin-left: 25%;">See More</a></div>
                </div>
            </div>
        
    </section>
    <!-- ##### Mag Posts Area End ##### -->
    <script>
        $('.carousel').carousel()
    </script>