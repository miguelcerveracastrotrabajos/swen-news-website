<%@page import="java.util.List"%>
<%@page import="Models.ModelType"%>
<h1>List User Types</h1>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-active-tab" data-toggle="pill" href="#pills-active" role="tab" aria-controls="pills-active" aria-selected="true">Active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-inactive-tab" data-toggle="pill" href="#pills-inactive" role="tab" aria-controls="pills-inactive" aria-selected="false">Inactive</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active table-responsive" id="pills-active" role="tabpanel" aria-labelledby="pills-active-tab">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Designation</th>
                <th scope="col">Description</th>
                <th scope="col">Timestamps</th>
                <th scope="col">Options</th>
              </tr>
            </thead>
            <tbody>
              <% ModelType t = new ModelType();
              List<ModelType> lstactive = (List<ModelType>)request.getAttribute("lstactive");
              if(lstactive!=null) {
                  List<ModelType> types = (List<ModelType>)request.getAttribute("types");
                  for (ModelType type : lstactive) {
              %>
              <form onsubmit="event.preventDefault(); updateTypeUser(new FormData($(this)[0]))">
                <tr>
                  <input type="hidden" name="typeid" value="<%= type.getTypeId()%>">
                  <td><input class="form-control typedesignation" name="typedesignation" value="<%= type.getTypeDesignation() %>"/></td>
                  <td><input class="form-control typedescription" name="typedescription" value="<%= type.getTypeDescription() %>"/></td>
                    <td class="timestampFont">    
                      <div>CA: <%= type.getCreated_at()%></div>
                      <div>UA: <%= type.getUpdated_at()%></div>
                  </td>
                  <td>
                      <button type="button" class="btn btn-danger" onclick="deleteTypeUser('<%= type.getTypeId()%>')">
                          <i class="fas fa-trash"></i>
                      </button>
                      <button type="submit" class="btn btn-warning">
                          <i class="fas fa-pen-square"></i
                      </button>  
                  </td>
                </tr>
              </form>
              <%
                  }
              }
              %>
             </tbody>
          </table>  
    </div>
    <div class="tab-pane fade table-responsive" id="pills-inactive" role="tabpanel" aria-labelledby="pills-inactive-tab">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Designation</th>
                <th scope="col">Description</th>
                <th scope="col">Timestamps</th>
                <th scope="col">Options</th>
              </tr>
            </thead>
            <tbody>
              <%
              List<ModelType> lstinactive = (List<ModelType>)request.getAttribute("lstinactive");
              if(lstinactive!=null) {
                  List<ModelType> types = (List<ModelType>)request.getAttribute("types");
                  for (ModelType type : lstinactive) {
              %>
              <form onsubmit="event.preventDefault(); activateTypeUser(new FormData($(this)[0]))">
                 <tr>
                  <input type="hidden" name="typeid" value="<%= type.getTypeId()%>">
                  <td><%= type.getTypeDesignation() %></td>
                  <td><%= type.getTypeDescription() %></td>
                  <td class="timestampFont">    
                    <div>CA: <%= type.getCreated_at()%></div>
                    <div>UA: <%= type.getUpdated_at()%></div>
                </td>
                  <td>
                      <button type="submit" class="btn btn-outline-success">
                          <i class="fas fa-check"></i>
                      </button>  
                  </td>
                </tr>
              </form>
              <%
                  }
              }
              %>
            </tbody>
          </table>
        </table>
    </div>
</div>