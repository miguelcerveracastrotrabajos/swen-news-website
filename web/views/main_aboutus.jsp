 
<%@page import="java.util.Collections"%>
<%@page import="Models.ModelUser"%>
<%@page import="java.util.List"%>
<%@page import="Models.ModelArticle"%>
<%@page import="Models.ModelMedia"%>
<!-- ##### Hero Area Start ##### -->
    <% 
        ModelArticle u = new ModelArticle();

        List<ModelArticle> lstactive = u.listActive(); 
        ModelArticle article = (ModelArticle)request.getAttribute("article");
    %>
    
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Mag Posts Area Start ##### -->
    <section class="mag-posts-area d-flex flex-wrap">

        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Left Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="post-sidebar-area left-sidebar mt-30 mb-30 bg-white box-shadow">
            <!-- Sidebar Widget -->
            <div class="single-sidebar-widget p-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>Most Popular</h5>
                </div>
                <div >
                    <%  
                       
                        if(lstactive!=null) {
                            List<ModelArticle> newlstactive =lstactive;
                            Collections.sort(newlstactive);
                            for (ModelArticle articleU : newlstactive) {     

                    %>
                    <!-- Single Blog Post -->
                    <div class="single-blog-post d-flex">
                        <div class="post-thumbnail">
                            <% if(new String(articleU.getImage().getMediaType()).equals("mp4")){ %> 
                                <video style="width:100%;">
                                    <source src="img/<%=articleU.getImage().getMediaId()+"."+articleU.getImage().getMediaType() %>" type="video/<%= articleU.getImage().getMediaType() %>">
                                 </video>
                            <% } else {%>
                                <img src="img/<%=articleU.getImage().getMediaId()+"."+articleU.getImage().getMediaType() %>">
                            <% } %>
                        </div>
                        <div class="post-content">
                            <a href="/ProjetoArtigos/main_article?id=<%= articleU.getArticleId()%>" class="post-title"> <%= articleU.getArticleTitle() %> </a>
                            <span class="mini-tag">
                                <i class="fa fa-eye" aria-hidden="true"></i><%= articleU.getArticleViews() %>
                            </span>
                        </div>
                    </div>
                    <%
                            }
                        }
                    %>
                </div>
            </div>
        </div>


        <!-- >>>>>>>>>>>>>>>>>>>>
             Main Posts Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="mag-posts-content mt-30 mb-30 p-30 box-shadow">
            <div class="col-12">
                <!-- About Us Content -->
                <div>
                    <!-- Section Title -->
                    <div class="section-heading">
                        <h5>About Us</h5>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consectetur mauris id scelerisque eleifend. Nunc vestibulum cursus quam at scelerisque. Aliquam quis varius orci, vel tincidunt est. Proin ac tincidunti, atmots interdum erat. Maecenas neque lorem, aliquet in tempus non, efficitur ac neque.</p>
                    <p>Phasellus elefend odio quis dolor pretium condimentu. Morbi quis mauris ipsum urna eu fermentum bentons Suspendisse auctor magna ac porta ornare. Fusce finibus nibh at lacinia lobortis.</p>
                    <ul>
                        <li><i class="fa fa-check"></i> Vivamus starlord finibus, dictum massa eget, suscipit metus nami at tristique elit started.</li>
                        <li><i class="fa fa-check"></i> Cras ipsum libero, suscipit vitamin tellus vitae, feugiat ultricies purus praesent gamora.</li>
                        <li><i class="fa fa-check"></i> Proin ex sem, ultrices drax the sit amet, facilisis destroyer et odio profession risusest.</li>
                        <li><i class="fa fa-check"></i> Morbi maximus mauris eget groot dignissim, in laoreet justo facilisis.</li>
                    </ul>
                    <img class="mt-15" src="img/bg-img/35.jpg" alt="">

                    <!-- Team Member Area -->
                    <div class="section-heading mt-30">
                        <h5>Our Team</h5>
                    </div>
                    <%
                        ModelUser p = new ModelUser();
                        List<ModelUser> lstactiveusers= p.listActive();
                        if(lstactiveusers!=null) {
                            for (ModelUser user : lstactiveusers) {  
                                if(new String(user.getType().getTypeId()).equals("ec29656b-1721-44c7-9b87-e9c8278bb45e")){
                     %>
                    <!-- Single Team Member -->
                    <div class="single-team-member d-flex align-items-center">
                        <div class="team-member-thumbnail">
                           <img src="img/<%=user.getMedia().getMediaId()+"."+user.getMedia().getMediaType() %>" class="userMiniImage">
                        </div>
                        <div class="team-member-content">
                            <h6>Mr. <%= user.getUserName()%></h6>
                            <span>Administrator</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consectetur mauris id scelerisque eleifend. Nunc vestibulum cursea quam at scelerisque.</p>
                        </div>
                    </div>
                    <%
                                }
                            }
                        }
                    %>
                </div>
            </div>
        </div>
        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Right Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="post-sidebar-area right-sidebar mt-30 mb-30 box-shadow bg-white">
                <!-- Sidebar Widget -->
                <div class="single-sidebar-widget p-30">
                    <!-- Social Followers Info -->
                    <div class="social-followers-info">
                        <!-- Facebook -->
                        <a href="#" class="facebook-fans"><i class="fa fa-facebook"></i> 4,360 <span>Fans</span></a>
                        <!-- Twitter -->
                        <a href="#" class="twitter-followers"><i class="fa fa-twitter"></i> 3,280 <span>Followers</span></a>
                        <!-- YouTube -->
                        <a href="#" class="youtube-subscribers"><i class="fa fa-youtube"></i> 1250 <span>Subscribers</span></a>
                        <!-- Google -->
                        <a href="#" class="google-followers"><i class="fa fa-google-plus"></i> 4,230 <span>Followers</span></a>
                    </div>
                </div>
    
                
    
                <!-- Sidebar Widget -->
                <div class="single-sidebar-widget p-30">
                    <!-- Section Title -->
                    <div class="section-heading">
                        <h5>About Us</h5>
                    </div>
    
                    <div class="newsletter-form">
                        <p>Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                
                        </p>
                    </div>
                     <div class="button-back-article"><a href="/ProjetoArtigos/main_aboutus" class="btn mag-btn w-100" style="    margin-left: 25%;">See More</a></div>
                </div>
            </div>
        
    </section>
    <!-- ##### Mag Posts Area End ##### -->
    <script>
        $('.carousel').carousel()
    </script>