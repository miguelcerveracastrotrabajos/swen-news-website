<form class="createArticle" enctype="multipart/form-data" onsubmit="event.preventDefault(); insertArticle(new FormData($(this)[0]))">
    <h1>Create an Article</h1>
    <div class="form-group">
        <label for="articleTitle">Article Title</label>
        <input class="form-control articletitle" id="articleTitle" name="articletitle" placeholder="Enter the Article Title">
    </div>
    <div class="form-group">
        <label for="articlesubtitle">Article Subtitle</label>
        <input class="form-control articlesubtitle" id="articlesubtitle" name="articlesubtitle" placeholder="Enter the Article Subtitle">
    </div>
    <label for="articlesfile">Article Image</label>
    <div class="input-group mb-3" id="articlesubtitle">
        <div class="custom-file">
          <input type="file" name="file" class="custom-file-input file" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
          <label class="custom-file-label" for="inputGroupFile01">Choose file to be the cover of the Article</label>
        </div>
     </div>
    <div class="form-group">
        <label for="articlebody">Article Body</label>
        <textarea class="form-control articlebody" rows="20" name="articlebody" style="resize:none;" placeholder="Enter the Article Body"></textarea>
    </div>
    <div class="row">
        <div class="col">
            <a href="/ProjetoArtigos/articles" class="btn btn-warning btn-lg" style="width:100%;">Cancel</a>
        </div>
        <div class="col">
            <button type="submit" class="btn btn-success btn-lg" style="width:100%;">Create</button>
        </div>
    </div>
</form>
