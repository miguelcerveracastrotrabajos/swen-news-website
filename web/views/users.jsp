<%@page import="java.util.List"%>
<%@page import="Models.ModelUser"%>
<%@page import="Models.ModelType"%>
<%@page import="Models.ModelMedia"%>
<h1>List Users</h1>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-active-tab" data-toggle="pill" href="#pills-active" role="tab" aria-controls="pills-active" aria-selected="true">Active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-inactive-tab" data-toggle="pill" href="#pills-inactive" role="tab" aria-controls="pills-inactive" aria-selected="false">Inactive</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active table-responsive" id="pills-active" role="tabpanel" aria-labelledby="pills-active-tab">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Picture</th>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">Type</th>
              <th scope="col">Timestamp</th>
              <th scope="col">Options</th>
            </tr>
          </thead>
          <tbody>
            <% ModelUser u = new ModelUser();
            List<ModelUser> lstactive = u.listActive();
            if(lstactive!=null) {
                for (ModelUser user : lstactive) {
            %>
            <tr>
                <td><img src="img/<%=user.getMedia().getMediaId()+"."+user.getMedia().getMediaType() %>" class="userMiniImage"></td>
                <td>    
                    <div><%= user.getUserName()%></div>
                    <div>(<%= user.getUserBirthday()%>)</div>
                </td>
                <td><%= user.getUserEmail() %></td>
                <td><%= user.getType().getTypeDesignation() %></td>
                <td class="timestampFont">    
                    <div>CA: <%= user.getCreated_at()%></div>
                    <div>UA: <%= user.getUpdated_at()%></div>
                </td>
                <td>
                    <a href="/ProjetoArtigos/users?id=<%= user.getUserId()%>" class="btn btn-success">
                        <i class="fas fa-eye"></i>
                    </a>
                </td>
            </tr>
            <%
                }
            }
            %>
          </tbody>
        </table>
    </div>
    <div class="tab-pane fade table-responsive" id="pills-inactive" role="tabpanel" aria-labelledby="pills-inactive-tab">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Picture</th>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">Type</th>
              <th scope="col">Timestamp</th>
              <th scope="col">Options</th>
            </tr>
          </thead>
          <tbody>
            <%
            List<ModelUser> lstinactive = u.listInactive();
            if(lstinactive!=null) {
                for (ModelUser user : lstinactive) {
            %>
            <form onsubmit="event.preventDefault(); activateUser(new FormData($(this)[0]))">
            <tr>
                <input name="userid" type="hidden" value="<%= user.getUserId() %>">
                <td><img src="img/<%=user.getMedia().getMediaId()+"."+user.getMedia().getMediaType() %>" class="userMiniImage"></td>
                <td>    
                    <div><%= user.getUserName()%></div>
                    <div>(<%= user.getUserBirthday()%>)</div>
                </td>
                <td><%= user.getUserEmail() %></td>
                <td><%= user.getType().getTypeDesignation() %></td>
                <td class="timestampFont">    
                    <div>CA: <%= user.getCreated_at()%></div>
                    <div>UA: <%= user.getUpdated_at()%></div>
                </td>
                <td>
                    <button type="submit" class="btn btn-outline-success">
                        <i class="fa fa-check"></i>
                    </button>
                </td>
            </tr>
            </form>
            <%
                }
            }
            %>
          </tbody>
        </table>
    </div>
</div>