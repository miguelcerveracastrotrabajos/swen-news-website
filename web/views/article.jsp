<%@page import="Models.ModelArticle"%>
<%@page import="Models.ModelUser"%>
<%
    ModelArticle article = (ModelArticle)request.getAttribute("article");
%>
<% if(new String(article.getImage().getMediaType()).equals("mp4")){ %> 
    <video height="624.5" controls>
        <source src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" type="video/<%= article.getImage().getMediaType() %>">
     </video>
<% } else {%>
    <img src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" style="width:100%;max-height:624.5px;">
<% } %>
<h1><%= article.getArticleTitle() %></h1>
<h3><%= article.getArticleSubtitle()%></h3>
<h4><%= article.getAuthor().getUserName() %> (<%= article.getUpdated_at() %>) </h4>
<span><%= article.getArticleBody()%></span>
<div class="row">
    <div class="col">
        <a href="/ProjetoArtigos/articles" class="btn btn-warning btn-lg" style="width:100%;">Cancel</a>
    </div>
    <div class="col">
        <a  href="/ProjetoArtigos/articles_update?id=<%= article.getArticleId()%>" class="btn btn-success btn-lg" style="width:100%;">Edit</a>
    </div>
</div>