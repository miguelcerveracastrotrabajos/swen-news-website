<%@page import="Models.ModelType"%>
<%@page import="java.util.List"%>
<form class="createUser" enctype="multipart/form-data" onsubmit="event.preventDefault(); insertUser(new FormData($(this)[0]))">
    <h1>Create an User</h1>
    <div class="form-group">
        <label for="username">Name</label>
        <input class="form-control username" id="username" name="username" placeholder="Enter the name">
    </div>
    <div class="form-group">
        <label for="userbirthday">Birthday</label>
        <input class="form-control userbirthday" id="userbirthday" name="userbirthday" placeholder="Enter the birthday">
    </div>
    <div class="form-group">
        <label for="useremail">Email</label>
        <input class="form-control useremail" id="useremail" name="useremail" placeholder="Enter the email">
    </div>
    <div class="form-group">
        <label for="userpassword">Password</label>
        <input class="form-control userpassword" id="userpassword" name="userpassword" placeholder="Enter the password" type="password">
    </div>
    <div class="form-group">
        <label for="fktype">Type</label>
         <select class="form-control fktype" name="fktype">
            <%
                ModelType t = new ModelType();
                List<ModelType> types = t.listActive();
                if(types!=null){
                    for (ModelType type : types) {
            %>
                <option value="<%= type.getTypeId()%>" ><%= type.getTypeDesignation() %></option>
            <%
                    }  
                }
            %>
          </select>
    </div>
    <label for="usersfile">Profile Image</label>
    <div class="input-group mb-3" id="articlesubtitle">
        <div class="custom-file">
          <input type="file" name="file" class="custom-file-input file" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
          <label class="custom-file-label" for="inputGroupFile01">Choose file to be the Profile Picture of the Profile</label>
        </div>
     </div>
    <div class="row">
        <div class="col">
            <a href="/ProjetoArtigos/users" class="btn btn-warning btn-lg" style="width:100%;">Cancel</a>
        </div>
        <div class="col">
            <button type="submit" class="btn btn-success btn-lg" style="width:100%;">Create</button>
        </div>
    </div>
</form>
