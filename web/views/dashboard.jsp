<%@page import="java.util.List"%>
<%@page import="Models.ModelLog"%>
<h1>Dashboard</h1>
<% 
    ModelLog log = new ModelLog();
    List<Integer> counters = log.getGraphsAndStats();
    StringBuffer values = new StringBuffer();
    for (int i = 0; i < counters.size(); ++i) {
        if (values.length() > 0) {
            values.append(',');
        }
        values.append('"').append(counters.get(i)).append('"');
    }
    List<String> articlesperuser = log.getArticlesPerUser();
    StringBuffer values2 = new StringBuffer();
    for (int i = 0; i < articlesperuser.size(); ++i) {
        if (values2.length() > 0) {
            values2.append(',');
        }
        values2.append('"').append(articlesperuser.get(i)).append('"');
    }
    List<String> userspertype = log.getUsersPerType();
    StringBuffer values3 = new StringBuffer();
    for (int i = 0; i < userspertype.size(); ++i) {
        if (values3.length() > 0) {
            values3.append(',');
        }
        values3.append('"').append(userspertype.get(i)).append('"');
    }
    List<String> viewsperarticle = log.getViewsPerArticle();
    StringBuffer values4 = new StringBuffer();
    for (int i = 0; i < viewsperarticle.size(); ++i) {
        if (values4.length() > 0) {
            values4.append(',');
        }
        values4.append('"').append(viewsperarticle.get(i)).append('"');
    }
    List<String> viewsperuser = log.getViewsPerUser();
    StringBuffer values5 = new StringBuffer();
    for (int i = 0; i < viewsperuser.size(); ++i) {
        if (values5.length() > 0) {
            values5.append(',');
        }
        values5.append('"').append(viewsperuser.get(i)).append('"');
    };
    List<String> logsperday = log.getLogsPerDay();
    StringBuffer values6 = new StringBuffer();
    for (int i = 0; i < logsperday.size(); ++i) {
        if (values6.length() > 0) {
            values6.append(',');
        }
        values6.append('"').append(logsperday.get(i)).append('"');
    }
    List<String> articlesperday = log.getArticlesPerDay();
    StringBuffer values7 = new StringBuffer();
    for (int i = 0; i < articlesperday.size(); ++i) {
        if (values7.length() > 0) {
            values7.append(',');
        }
        values7.append('"').append(articlesperday.get(i)).append('"');
    }
%>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-comparisons-tab" data-toggle="pill" href="#pills-comparisons" role="tab" aria-controls="pills-comparisons" aria-selected="true">Comparisons</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-counters-tab" data-toggle="pill" href="#pills-counters" role="tab" aria-controls="pills-counters" aria-selected="false">Counters and Avg</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-liners-tab" data-toggle="pill" href="#pills-liners" role="tab" aria-controls="pills-liners" aria-selected="false">Liners</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-timeline-tab" data-toggle="pill" href="#pills-timeline" role="tab" aria-controls="pills-timeline" aria-selected="false">Timelines</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="pills-comparisons" role="tabpanel" aria-labelledby="pills-comparisons-tab">
        <table class="table">
            <tr>
                <th style="text-align:center;" colspan="4">Model of Logs</th><th style="text-align:center;" colspan="4">Type of Logs</th>
            </tr>
            <tr>
                <td colspan="2">
                    <canvas id="chart1"></canvas>
                </td>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Logs:</div>
                        <div>Medias:</div>
                        <div>Types:</div>
                        <div>Articles:</div>
                        <div>Users:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(0) %> (100%)</div> 
                        <div><%= counters.get(1) %> (<% double d=((double)counters.get(1))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(2) %> (<% d=((double)counters.get(2))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(3) %> (<% d=((double)counters.get(3))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(4) %> (<% d=((double)counters.get(4))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(5) %> (<% d=((double)counters.get(5))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
                <td colspan="2">
                    <canvas id="chart2"></canvas>
                </td>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Errors:</div>
                        <div>Actions:</div>
                        <div>Views:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(0) %> (100%)</div> 
                        <div><%= counters.get(6) %> (<% d=((double)counters.get(6))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(7) %> (<% d=((double)counters.get(7))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(8) %> (<% d=((double)counters.get(8))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
            <tr>
                <th style="text-align:center;" colspan="4">Status Articles</th><th style="text-align:center;" colspan="4">Uploaded Elements</th>
            </tr>
            <tr>
                <td colspan="2">
                    <canvas id="chart3"></canvas>
                </td>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Active</div>
                        <div>Inactive:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(9) %> (100%)</div> 
                        <div><%= counters.get(10) %> (<% d=((double)counters.get(10))  / (int)counters.get(9); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(11) %> (<% d=((double)counters.get(11))  / (int)counters.get(9); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
                <td colspan="2">
                    <canvas id="chart4"></canvas>
                </td>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Videos:</div>
                        <div>Images:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(12) %> (100%)</div> 
                        <div><%= counters.get(13) %> (<% d=((double)counters.get(13))  / (int)counters.get(12); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(14) %> (<% d=((double)counters.get(14))  / (int)counters.get(12); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
            <tr>
                <th style="text-align:center;" colspan="4">Status User Type</th><th style="text-align:center;" colspan="4">Status User</th>
            </tr>
            <tr>
                <td colspan="2">
                    <canvas id="chart5"></canvas>
                </td>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Active</div>
                        <div>Inactive:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(15) %> (100%)</div> 
                        <div><%= counters.get(16) %> (<% d=((double)counters.get(16))  / (int)counters.get(15); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(17) %> (<% d=((double)counters.get(17))  / (int)counters.get(15); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
                <td colspan="2">
                    <canvas id="chart6"></canvas>
                </td>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Active</div>
                        <div>Inactive:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(18) %> (100%)</div> 
                        <div><%= counters.get(19) %> (<% d=((double)counters.get(19))  / (int)counters.get(18); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(20) %> (<% d=((double)counters.get(20))  / (int)counters.get(18); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
            <tr>
                <th style="text-align:center;" colspan="4">Type of Actions</th><th style="text-align:center;" colspan="4">Actions in Articles</th>
            </tr>
            <tr>
                <td colspan="2">
                    <canvas id="chart7"></canvas>
                </td>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Creates:</div>
                        <div>Updates:</div>
                        <div>Deletes:</div>
                        <div>Activates:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(7) %> (100%)</div> 
                        <div><%= counters.get(22) %> (<% d=((double)counters.get(22))  / (int)counters.get(7); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(23) %> (<% d=((double)counters.get(23))  / (int)counters.get(7); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(24) %> (<% d=((double)counters.get(24))  / (int)counters.get(7); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(25) %> (<% d=((double)counters.get(25))  / (int)counters.get(7); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
                <td colspan="2">
                    <canvas id="chart8"></canvas>
                </td>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Creates:</div>
                        <div>Updates:</div>
                        <div>Deletes:</div>
                        <div>Activates:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <% int total = counters.get(26)+counters.get(27)+counters.get(28)+counters.get(29); %>
                        <div><%= total %> (100%)</div> 
                        <div><%= counters.get(26) %> (<% d=((double)counters.get(26))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(27) %> (<% d=((double)counters.get(27))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(28) %> (<% d=((double)counters.get(28))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(29) %> (<% d=((double)counters.get(29))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
            <tr>
                <th style="text-align:center;" colspan="4">Actions in User Types</th><th style="text-align:center;" colspan="4">Actions in Users</th>
            </tr>
            <tr>
                <td colspan="2">
                    <canvas id="chart9"></canvas>
                </td>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Creates:</div>
                        <div>Updates:</div>
                        <div>Deletes:</div>
                        <div>Activates:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <% total = counters.get(30)+counters.get(27)+counters.get(28)+counters.get(29); %>
                        <div><%= total %> (100%)</div> 
                        <div><%= counters.get(30) %> (<% d=((double)counters.get(30))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(31) %> (<% d=((double)counters.get(31))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(32) %> (<% d=((double)counters.get(32))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(33) %> (<% d=((double)counters.get(33))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
                 <td colspan="2">
                    <canvas id="chart10"></canvas>
                </td>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Creates:</div>
                        <div>Updates:</div>
                        <div>Deletes:</div>
                        <div>Activates:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <% total = counters.get(34)+counters.get(35)+counters.get(36)+counters.get(37); %>
                        <div><%= total %> (100%)</div> 
                        <div><%= counters.get(34) %> (<% d=((double)counters.get(34))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(35) %> (<% d=((double)counters.get(35))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(36) %> (<% d=((double)counters.get(36))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(37) %> (<% d=((double)counters.get(37))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
            <tr>
                <th style="text-align:center;" colspan="4">Authentication</th><th style="text-align:center;" colspan="4">Authentication Failed By</th>
            </tr>
            <tr>
                <td colspan="2">
                    <canvas id="chart17"></canvas>
                </td>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Success:</div>
                        <div>Failed:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(38) %> (100%)</div> 
                        <div><%= counters.get(39) %> (<% d=((double)counters.get(39))  / (int)counters.get(38); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(40) %> (<% d=((double)counters.get(40))  / (int)counters.get(38); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
                 <td colspan="2">
                    <canvas id="chart18"></canvas>
                </td>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>By Email:</div>
                        <div>By Password:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(40) %> (100%)</div> 
                        <div><%= counters.get(41) %> (<% d=((double)counters.get(41))  / (int)counters.get(40); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(42) %> (<% d=((double)counters.get(42))  / (int)counters.get(40); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="tab-pane fade" id="pills-counters" role="tabpanel" aria-labelledby="pills-counters-tab">
        <table class="table col-3" style="display: inline;">
            <tr>
                <th style="text-align: center;" colspan="2">Model of Logs</th>
            </tr>
            <tr>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Logs:</div>
                        <div>Medias:</div>
                        <div>Types:</div>
                        <div>Articles:</div>
                        <div>Users:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(0) %> (100%)</div> 
                        <div><%= counters.get(1) %> (<% d=((double)counters.get(1))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(2) %> (<% d=((double)counters.get(2))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(3) %> (<% d=((double)counters.get(3))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(4) %> (<% d=((double)counters.get(4))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(5) %> (<% d=((double)counters.get(5))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table col-3" style="display: inline;">
            <tr>
                <th style="text-align: center;" colspan="2">Type of Logs</th>
            </tr>
            <tr>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Errors:</div>
                        <div>Actions:</div>
                        <div>Views:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(0) %> (100%)</div> 
                        <div><%= counters.get(6) %> (<% d=((double)counters.get(6))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(7) %> (<% d=((double)counters.get(7))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(8) %> (<% d=((double)counters.get(8))  / (int)counters.get(0); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table col-3" style="display: inline;">
            <tr>
                <th style="text-align: center;" colspan="2">Status Articles</th>
            </tr>
            <tr>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Active</div>
                        <div>Inactive:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(9) %> (100%)</div> 
                        <div><%= counters.get(10) %> (<% d=((double)counters.get(10))  / (int)counters.get(9); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(11) %> (<% d=((double)counters.get(11))  / (int)counters.get(9); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table col-3" style="display: inline;">
            <tr>
                <th style="text-align: center;" colspan="2">Uploaded Elements</th>
            </tr>
            <tr>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Videos:</div>
                        <div>Images:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(12) %> (100%)</div> 
                        <div><%= counters.get(13) %> (<% d=((double)counters.get(13))  / (int)counters.get(12); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(14) %> (<% d=((double)counters.get(14))  / (int)counters.get(12); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table col-3" style="display: inline;">
            <tr>
                <th style="text-align: center;" colspan="2">Status User Type</th>
            </tr>
            <tr>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Active</div>
                        <div>Inactive:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(15) %> (100%)</div> 
                        <div><%= counters.get(16) %> (<% d=((double)counters.get(16))  / (int)counters.get(15); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(17) %> (<% d=((double)counters.get(17))  / (int)counters.get(15); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table col-3" style="display: inline;">
            <tr>
                <th style="text-align: center;" colspan="2">Status User</th>
            </tr>
            <tr>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Active</div>
                        <div>Inactive:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(18) %> (100%)</div> 
                        <div><%= counters.get(19) %> (<% d=((double)counters.get(19))  / (int)counters.get(18); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(20) %> (<% d=((double)counters.get(20))  / (int)counters.get(18); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table col-3" style="display: inline;">
            <tr>
                <th style="text-align: center;" colspan="2">Type of Actions</th>
            </tr>
            <tr>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Creates:</div>
                        <div>Updates:</div>
                        <div>Deletes:</div>
                        <div>Activates:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(7) %> (100%)</div> 
                        <div><%= counters.get(22) %> (<% d=((double)counters.get(22))  / (int)counters.get(7); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(23) %> (<% d=((double)counters.get(23))  / (int)counters.get(7); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(24) %> (<% d=((double)counters.get(24))  / (int)counters.get(7); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(25) %> (<% d=((double)counters.get(25))  / (int)counters.get(7); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table col-3" style="display: inline;">
            <tr>
                <th style="text-align: center;" colspan="2">Actions in Articles</th>
            </tr>
            <tr>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Creates:</div>
                        <div>Updates:</div>
                        <div>Deletes:</div>
                        <div>Activates:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <% total = counters.get(26)+counters.get(27)+counters.get(28)+counters.get(29); %>
                        <div><%= total %> (100%)</div> 
                        <div><%= counters.get(26) %> (<% d=((double)counters.get(26))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(27) %> (<% d=((double)counters.get(27))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(28) %> (<% d=((double)counters.get(28))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(29) %> (<% d=((double)counters.get(29))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table col-3" style="display: inline;">
            <tr>
                <th style="text-align: center;" colspan="2">Actions in User Types</th>
            </tr>
            <tr>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Creates:</div>
                        <div>Updates:</div>
                        <div>Deletes:</div>
                        <div>Activates:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <% total = counters.get(30)+counters.get(27)+counters.get(28)+counters.get(29); %>
                        <div><%= total %> (100%)</div> 
                        <div><%= counters.get(30) %> (<% d=((double)counters.get(30))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(31) %> (<% d=((double)counters.get(31))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(32) %> (<% d=((double)counters.get(32))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(33) %> (<% d=((double)counters.get(33))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table col-3" style="display: inline;">
            <tr>
                <th style="text-align: center;" colspan="2">Actions in Users</th>
            </tr>
            <tr>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Creates:</div>
                        <div>Updates:</div>
                        <div>Deletes:</div>
                        <div>Activates:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <% total = counters.get(34)+counters.get(35)+counters.get(36)+counters.get(37); %>
                        <div><%= total %> (100%)</div> 
                        <div><%= counters.get(34) %> (<% d=((double)counters.get(34))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(35) %> (<% d=((double)counters.get(35))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(36) %> (<% d=((double)counters.get(36))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(37) %> (<% d=((double)counters.get(37))  / (int)total; out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table col-3" style="display: inline;">
            <tr>
                <th style="text-align: center;" colspan="2">Authentication</th>
            </tr>
            <tr>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Success:</div>
                        <div>Failed:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(38) %> (100%)</div> 
                        <div><%= counters.get(39) %> (<% d=((double)counters.get(39))  / (int)counters.get(38); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(40) %> (<% d=((double)counters.get(40))  / (int)counters.get(38); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table col-3" style="display: inline;">
            <tr>
                <th style="text-align: center;" colspan="2">Authentication Failed By</th>
            </tr>
            <tr>
                <td>
                    <div>
                        <div>Total:</div> 
                        <div>Email:</div>
                        <div>Password:</div>
                    </div>
                </td>
                <td>
                    <div>
                        <div><%= counters.get(40) %> (100%)</div> 
                        <div><%= counters.get(41) %> (<% d=((double)counters.get(41))  / (int)counters.get(40); out.println( Math.round(d * 100.0)); %>%)</div>
                        <div><%= counters.get(42) %> (<% d=((double)counters.get(42))  / (int)counters.get(40); out.println( Math.round(d * 100.0)); %>%)</div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table">
            <tr>
                <th colspan="4" style="text-align:center;">Averages</th>
            </tr>
            <tr>
                <td>Average Articles Per User</td>
                <td id="articlesperuser">0</td>
            </tr>
            <tr>
                <td>Average User per Type</td>
                <td id="userspertype">0</td>
            </tr>
            <tr>
                <td>Average Views per Article</td>
                <td id="viewsperarticle">0</td>
            </tr>
            <tr>
                <td>Average Views per User</td>
                <td id="viewsperuser">0</td>
            </tr>
            <tr>
                <td>Average User Age</td>
                <td><%= counters.get(21) %></td>
            </tr>
            <tr>
                <td>Average Logs per Day</td>
                <td id="logsperday">0</td>
            </tr>
            <tr>
                <td>Average Articles per Day</td>
                <td id="articlesperday">0</td>
            </tr>
        </table>
    </div>
    <div class="tab-pane fade" id="pills-liners" role="tabpanel" aria-labelledby="pills-liners-tab">
        <h3>Articles Per User</h3>
        <canvas id="chart11"></canvas>
        <h3>User Per Type</h3>
        <canvas id="chart12"></canvas>
        <h3>Views Per Article</h3>
        <canvas id="chart13"></canvas>
        <h3>Views Per User</h3>
        <canvas id="chart14"></canvas>
    </div>
    <div class="tab-pane fade" id="pills-timeline" role="tabpanel" aria-labelledby="pills-timeline-tab">
        <h3>Logs per Day</h3>
        <canvas id="chart15"></canvas>
        <h3>Articles per Day</h3>
        <canvas id="chart16"></canvas>
    </div>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script>
    var colors= [
                "#63b598", "#ce7d78", "#ea9e70", "#a48a9e", "#c6e1e8", "#648177" ,"#0d5ac1" ,
                "#f205e6" ,"#1c0365" ,"#14a9ad" ,"#4ca2f9" ,"#a4e43f" ,"#d298e2" ,"#6119d0",
                "#d2737d" ,"#c0a43c" ,"#f2510e" ,"#651be6" ,"#79806e" ,"#61da5e" ,"#cd2f00" ,
                "#9348af" ,"#01ac53" ,"#c5a4fb" ,"#996635","#b11573" ,"#4bb473" ,"#75d89e" ,
                "#2f3f94" ,"#2f7b99" ,"#da967d" ,"#34891f" ,"#b0d87b" ,"#ca4751" ,"#7e50a8" ,
                "#c4d647" ,"#e0eeb8" ,"#11dec1" ,"#289812" ,"#566ca0" ,"#ffdbe1" ,"#2f1179" ,
                "#935b6d" ,"#916988" ,"#513d98" ,"#aead3a", "#9e6d71", "#4b5bdc", "#0cd36d",
                "#250662", "#cb5bea", "#228916", "#ac3e1b", "#df514a", "#539397", "#880977",
                "#f697c1", "#ba96ce", "#679c9d", "#c6c42c", "#5d2c52", "#48b41b", "#e1cf3b",
                "#5be4f0", "#57c4d8", "#a4d17a", "#225b8", "#be608b", "#96b00c", "#088baf",
                "#f158bf", "#e145ba", "#ee91e3", "#05d371", "#5426e0", "#4834d0", "#802234",
                "#6749e8", "#0971f0", "#8fb413", "#b2b4f0", "#c3c89d", "#c9a941", "#41d158",
                "#fb21a3", "#51aed9", "#5bb32d", "#807fb", "#21538e", "#89d534", "#d36647",
                "#7fb411", "#0023b8", "#3b8c2a", "#986b53", "#f50422", "#983f7a", "#ea24a3",
                "#79352c", "#521250", "#c79ed2", "#d6dd92", "#e33e52", "#b2be57", "#fa06ec",
                "#1bb699", "#6b2e5f", "#64820f", "#1c271", "#21538e", "#89d534", "#d36647",
                "#7fb411", "#0023b8", "#3b8c2a", "#986b53", "#f50422", "#983f7a", "#ea24a3",
                "#79352c", "#521250", "#c79ed2", "#d6dd92", "#e33e52", "#b2be57", "#fa06ec",
                "#1bb699", "#6b2e5f", "#64820f", "#1c271", "#9cb64a", "#996c48", "#9ab9b7",
                "#06e052", "#e3a481", "#0eb621", "#fc458e", "#b2db15", "#aa226d", "#792ed8",
                "#73872a", "#520d3a", "#cefcb8", "#a5b3d9", "#7d1d85", "#c4fd57", "#f1ae16",
                "#8fe22a", "#ef6e3c", "#243eeb", "#1dc18", "#dd93fd", "#3f8473", "#e7dbce",
                "#421f79", "#7a3d93", "#635f6d", "#93f2d7", "#9b5c2a", "#15b9ee", "#0f5997",
                "#409188", "#911e20", "#1350ce", "#10e5b1", "#fff4d7", "#cb2582", "#ce00be",
                "#32d5d6", "#17232", "#608572", "#c79bc2", "#00f87c", "#77772a", "#6995ba",
                "#fc6b57", "#f07815", "#8fd883", "#060e27", "#96e591", "#21d52e", "#d00043",
                "#b47162", "#1ec227", "#4f0f6f", "#1d1d58", "#947002", "#bde052", "#e08c56",
                "#28fcfd", "#bb09b", "#36486a", "#d02e29", "#1ae6db", "#3e464c", "#a84a8f",
                "#911e7e", "#3f16d9", "#0f525f", "#ac7c0a", "#b4c086", "#c9d730", "#30cc49",
                "#3d6751", "#fb4c03", "#640fc1", "#62c03e", "#d3493a", "#88aa0b", "#406df9",
                "#615af0", "#4be47", "#2a3434", "#4a543f", "#79bca0", "#a8b8d4", "#00efd4",
                "#7ad236", "#7260d8", "#1deaa7", "#06f43a", "#823c59", "#e3d94c", "#dc1c06",
                "#f53b2a", "#b46238", "#2dfff6", "#a82b89", "#1a8011", "#436a9f", "#1a806a",
                "#4cf09d", "#c188a2", "#67eb4b", "#b308d3", "#fc7e41", "#af3101", "#ff065",
                "#71b1f4", "#a2f8a5", "#e23dd0", "#d3486d", "#00f7f9", "#474893", "#3cec35",
                "#1c65cb", "#5d1d0c", "#2d7d2a", "#ff3420", "#5cdd87", "#a259a4", "#e4ac44",
                "#1bede6", "#8798a4", "#d7790f", "#b2c24f", "#de73c2", "#d70a9c", "#25b67",
                "#88e9b8", "#c2b0e2", "#86e98f", "#ae90e2", "#1a806b", "#436a9e", "#0ec0ff",
                "#f812b3", "#b17fc9", "#8d6c2f", "#d3277a", "#2ca1ae", "#9685eb", "#8a96c6",
                "#dba2e6", "#76fc1b", "#608fa4", "#20f6ba", "#07d7f6", "#dce77a", "#77ecca"
        ];
    var counters =  [ <%= values %> ];
    for(var i=0; i<counters.length; i++){
        counters[i]= parseInt(counters[i]);
    }
    var articlesperuser =  [ <%= values2 %> ];
    var articlesperuservalue = [];
    console.log();
    for(var i=0; i<articlesperuser.length; i++){
        var array = articlesperuser[i].split("_");
        articlesperuser[i]= array[0];
        articlesperuservalue[i]=parseInt(array[1]);
    }
    var userspertype =  [ <%= values3 %> ];
    var userspertypevalue = [];
    for(var i=0; i<userspertype.length; i++){
        var array = userspertype[i].split("_");
        userspertype[i]= array[0];
        userspertypevalue[i]=parseInt(array[1]);
    }
    var viewsperarticle =  [ <%= values4 %> ];
    var viewsperarticlevalue = [];
    for(var i=0; i<viewsperarticle.length; i++){
        var array = viewsperarticle[i].split("_");
        viewsperarticle[i]= array[0].substring(0,15);
        viewsperarticlevalue[i]=parseInt(array[1]);
    }
    var viewsperuser =  [ <%= values5 %> ];
    var viewsperuservalue = [];
    for(var i=0; i<viewsperuser.length; i++){
        var array = viewsperuser[i].split("_");
        viewsperuser[i]= array[0].substring(0,15);
        viewsperuservalue[i]=parseInt(array[1]);
    }
    var logsperday =  [ <%= values6 %> ];
    var logsperdayvalue = [];
    for(var i=0; i<logsperday.length; i++){
        var array = logsperday[i].split("_");
        logsperday[i]= array[0].substring(0,15);
        logsperdayvalue[i]=parseInt(array[1]);
    }
    var articlesperday =  [ <%= values7 %> ];
    var articlesperdayvalue = [];
    for(var i=0; i<articlesperday.length; i++){
        var array = articlesperday[i].split("_");
        articlesperday[i]= array[0].substring(0,15);
        articlesperdayvalue[i]=parseInt(array[1]);
    }
    //ADD AVERAGES
    console.log(averageArray(articlesperuservalue), articlesperuservalue)
    document.getElementById('articlesperuser').innerHTML  = Math.round(averageArray(articlesperuservalue) * 100) / 100;
    document.getElementById('userspertype').innerHTML  = Math.round(averageArray(userspertypevalue)* 100) / 100;
    document.getElementById('viewsperarticle').innerHTML  = Math.round(averageArray(viewsperarticlevalue)* 100) / 100;
    document.getElementById('viewsperuser').innerHTML  = Math.round(averageArray(viewsperuservalue)* 100) / 100;
    document.getElementById('logsperday').innerHTML  = Math.round(averageArray(logsperdayvalue)* 100) / 100;
    document.getElementById('articlesperday').innerHTML  = Math.round(averageArray(articlesperdayvalue)* 100) / 100;
    
    
    var chart1 = document.getElementById('chart1').getContext('2d');
    var chart1 = new Chart(chart1, {
    // The type of chart we want to create
    type: 'doughnut',
    // The data for our dataset
    data: {
        labels: ["Logs", "Medias", "Types", "Articles", "Users"],
        datasets: [{
            label: "Model of Logs",
            backgroundColor: colors,
            borderColor: 'rgb(255, 255, 255)',
            data: [counters[1], counters[2], counters[3], counters[4], counters[5]],
        }]
        },
    });
    var chart2 = document.getElementById('chart2').getContext('2d');
    var chart2 = new Chart(chart2, {
    // The type of chart we want to create
    type: 'doughnut',
    // The data for our dataset
    data: {
        labels: ["Errors", "Actions", "View"],
        datasets: [{
            label: "Type of Logs",
            backgroundColor: colors,
            borderColor: 'rgb(255, 255, 255)',
            data: [counters[6], counters[7], counters[8]],
        }]
        },
    });
    var chart3 = document.getElementById('chart3').getContext('2d');
    var chart3 = new Chart(chart3, {
    // The type of chart we want to create
    type: 'doughnut',
    // The data for our dataset
    data: {
        labels: ["Active", "Inactive"],
        datasets: [{
            label: "Status Articles",
            backgroundColor: colors,
            borderColor: 'rgb(255, 255, 255)',
            data: [counters[10], counters[11]],
        }]
        },
    });
    var chart4 = document.getElementById('chart4').getContext('2d');
    var chart4 = new Chart(chart4, {
    // The type of chart we want to create
    type: 'doughnut',
    // The data for our dataset
    data: {
        labels: ["Videos", "Images"],
        datasets: [{
            label: "Uploaded Elements",
            backgroundColor: colors,
            borderColor: 'rgb(255, 255, 255)',
            data: [counters[13], counters[14]],
        }]
        },
    });
    var chart5 = document.getElementById('chart5').getContext('2d');
    var chart5 = new Chart(chart5, {
    // The type of chart we want to create
    type: 'doughnut',
    // The data for our dataset
    data: {
        labels: ["Active", "Inactive"],
        datasets: [{
            label: "Status User Type",
            backgroundColor: colors,
            borderColor: 'rgb(255, 255, 255)',
            data: [counters[16], counters[17]],
        }]
        },
    });
    var chart6 = document.getElementById('chart6').getContext('2d');
    var chart6 = new Chart(chart6, {
    // The type of chart we want to create
    type: 'doughnut',
    // The data for our dataset
    data: {
        labels: ["Active", "Inactive"],
        datasets: [{
            label: "Status User",
            backgroundColor: colors,
            borderColor: 'rgb(255, 255, 255)',
            data: [counters[19], counters[20]],
        }]
        },
    });
    var chart7 = document.getElementById('chart7').getContext('2d');
    var chart7 = new Chart(chart7, {
    // The type of chart we want to create
    type: 'doughnut',
    // The data for our dataset
    data: {
        labels: ["Creates", "Updates", "Deletes", "Activates"],
        datasets: [{
            label: "Type of Actions",
            backgroundColor: colors,
            borderColor: 'rgb(255, 255, 255)',
            data: [counters[22], counters[23], counters[24], counters[25]],
        }]
        },
    });
    var chart8 = document.getElementById('chart8').getContext('2d');
    var chart8 = new Chart(chart8, {
    // The type of chart we want to create
    type: 'doughnut',
    // The data for our dataset
    data: {
        labels: ["Creates", "Updates", "Deletes", "Activates"],
        datasets: [{
            label: "Actions in Articles",
            backgroundColor: colors,
            borderColor: 'rgb(255, 255, 255)',
            data: [counters[26], counters[27], counters[28], counters[29]],
        }]
        },
    });
    var chart9 = document.getElementById('chart9').getContext('2d');
    var chart9 = new Chart(chart9, {
    // The type of chart we want to create
    type: 'doughnut',
    // The data for our dataset
    data: {
        labels: ["Creates", "Updates", "Deletes", "Activates"],
        datasets: [{
            label: "Actions in User types",
            backgroundColor: colors,
            borderColor: 'rgb(255, 255, 255)',
            data: [counters[30], counters[31], counters[32], counters[33]],
        }]
        },
    });
    var chart10 = document.getElementById('chart10').getContext('2d');
    var chart10 = new Chart(chart10, {
    // The type of chart we want to create
    type: 'doughnut',
    // The data for our dataset
    data: {
        labels: ["Creates", "Updates", "Deletes", "Activates"],
        datasets: [{
            label: "Actions in Users",
            backgroundColor: colors,
            borderColor: 'rgb(255, 255, 255)',
            data: [counters[34], counters[35], counters[36], counters[37]],
        }]
        },
    });
    var chart11 = document.getElementById("chart11").getContext('2d');
    var chart11 = new Chart(chart11, {
        type: 'horizontalBar',
        data: {
            labels: articlesperuser,
            datasets: [{
                label:"Articles per User",
                data: articlesperuservalue,
                backgroundColor: colors,
                borderColor: 'rgb(255, 255, 255)',
                borderWidth: 2,
                lineTension:0.1
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    var chart12 = document.getElementById("chart12").getContext('2d');
    var chart12 = new Chart(chart12, {
        type: 'horizontalBar',
        data: {
            labels: userspertype,
            datasets: [{
                label:"User per Type",
                data: userspertypevalue,
                backgroundColor: colors,
                borderColor: 'rgb(255, 255, 255)',
                borderWidth: 2,
                lineTension:0.1
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    var chart13 = document.getElementById("chart13").getContext('2d');
    var chart13 = new Chart(chart13, {
        type: 'horizontalBar',
        data: {
            labels: viewsperarticle,
            datasets: [{
                label:"Views per Article",
                data: viewsperarticlevalue,
                backgroundColor: colors,
                borderColor: 'rgb(255, 255, 255)',
                borderWidth: 2,
                lineTension:0.1
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    var chart14 = document.getElementById("chart14").getContext('2d');
    var chart14 = new Chart(chart14, {
        type: 'horizontalBar',
        data: {
            labels: viewsperuser,
            datasets: [{
                label:"Views per User",
                data: viewsperuservalue,
                backgroundColor: colors,
                borderColor: 'rgb(255, 255, 255)',
                borderWidth: 2,
                lineTension:0.1
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    var chart15 = document.getElementById("chart15").getContext('2d');
    var chart15 = new Chart(chart15, {
        type: 'line',
        data: {
            labels: logsperday,
            datasets: [{
                label:"Logs per day",
                data: logsperdayvalue,
                borderColor: colors,
                fill:false,
                lineTension:0.1
            }]
        }
    });
    var chart16 = document.getElementById("chart16").getContext('2d');
    var chart16 = new Chart(chart16, {
        type: 'line',
        data: {
            labels: articlesperday,
            datasets: [{
                label:"Articles per day",
                data: articlesperdayvalue,
                borderColor: colors,
                fill:false,
                lineTension:0.1
            }]
        },
         options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    var chart17 = document.getElementById('chart17').getContext('2d');
    var chart17 = new Chart(chart17, {
    // The type of chart we want to create
    type: 'doughnut',
    // The data for our dataset
    data: {
        labels: ["Success", "Failed"],
        datasets: [{
            label: "Status User Type",
            backgroundColor: colors,
            borderColor: 'rgb(255, 255, 255)',
            data: [counters[39], counters[40]],
        }]
        },
    });
    var chart18 = document.getElementById('chart18').getContext('2d');
    var chart18 = new Chart(chart18, {
    // The type of chart we want to create
    type: 'doughnut',
    // The data for our dataset
    data: {
        labels: ["By Email", "By Password"],
        datasets: [{
            label: "Status User Type",
            backgroundColor: colors,
            borderColor: 'rgb(255, 255, 255)',
            data: [counters[41], counters[42]],
        }]
        },
    });
    //HELPER
    function averageArray(elmt){
        var sum = 0;
        for( var i = 0; i < elmt.length; i++ ){
            sum += parseInt( elmt[i], 10 ); //don't forget to add the base
        }

        var avg = sum/elmt.length;
        return avg;
    }
    
</script>