<%@page import="Models.ModelUser"%>
<%@page import="Models.ModelMedia"%>
<%@page import="Models.ModelType"%>
<%
    ModelUser user = (ModelUser)request.getAttribute("user");
%>
<div class="container" style="padding-top: 3%; width: 80%;padding-left: 10%;padding-right: 10%;">
  <div class="row col-12" style="padding-bottom: 2%;">
      <div class="col-3" style="margin-left: -30px;">
        <img src="img/<%=user.getMedia().getMediaId()+"."+user.getMedia().getMediaType() %>" style="width:250px;height:250px">
      </div>
    <div class="col-9" style="padding-left: 20%;">
      <h2><%= user.getUserName()%></h2>
        <h4><%= user.getUserEmail()%></h4>
        <h4><%= user.getUserBirthday()%></h4>
        <h5>User Type: <%= user.getType().getTypeDesignation() %></h5>
        <h7>
          <div>Last Update: <%= user.getType().getTypeDesignation() %></div>
          <div>Since: <%= user.getCreated_at()%></div>
        </h7>
    </div>
  </div>
  <div class="row" style="width: 75%;">
      <a href="/ProjetoArtigos/users" class="btn btn-danger btn-lg" style="width:100%; margin-bottom: 2%;">Cancel</a>            
      <a href="/ProjetoArtigos/users_update?id=<%= user.getUserId()%>" class="btn btn-success btn-lg" style="width:100%;">Edit</a>     
  </div>  
</div>