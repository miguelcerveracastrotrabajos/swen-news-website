<%@page import="Models.ModelArticle"%>
<% ModelArticle article = (ModelArticle)request.getAttribute("article"); %>    
<form class="updateArticle" enctype="multipart/form-data" onsubmit="event.preventDefault(); updateArticle(new FormData($(this)[0]))">
    <h1>Update an Article</h1>
    <% if(new String(article.getImage().getMediaType()).equals("mp4")){ %> 
        <video height="624.5" controls>
            <source src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" type="video/<%= article.getImage().getMediaType() %>">
         </video>
    <% } else {%>
        <img src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" style="width:100%;max-height:624.5px;">
    <% } %>
    <input class="articleid" type="hidden" name="articleid" value="<%= article.getArticleId() %>">
    <div class="form-group">
        <label for="articleTitle">Article Title</label>
        <input class="form-control articletitle" id="articleTitle" name="articletitle" placeholder="Enter the Article Title" value="<%= article.getArticleTitle() %>">
    </div>
    <div class="form-group">
        <label for="articlesubtitle">Article Subtitle</label>
        <input class="form-control articlesubtitle" id="articlesubtitle" name="articlesubtitle" placeholder="Enter the Article Subtitle" value="<%= article.getArticleSubtitle()%>">
    </div>
    <label for="articlesfile">Article Image</label>
    <div class="input-group mb-3" id="articlesubtitle">
        <div class="custom-file">
          <input type="file" name="file" class="custom-file-input file" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
          <label class="custom-file-label" for="inputGroupFile01">Choose file to be the cover of the Article</label>
        </div>
     </div>
    <div class="form-group">
        <label for="articlebody">Article Body</label>
        <textarea class="form-control articlebody" rows="20" name="articlebody" style="resize:none;" placeholder="Enter the Article Body"><%= article.getArticleBody() %></textarea>
    </div>
    <div class="row">
        <div class="col">
            <button type="button" class="btn btn-danger btn-lg " style="width:100%;" onclick="event.preventDefault(); deleteArticle(new FormData($('.updateArticle')[0]))" style="width:100%;">Delete</a>
        </div>
        <div class="col">
            <button type="submit" class="btn btn-success btn-lg" style="width:100%;">Edit</button>
        </div>
    </div>
</form>
