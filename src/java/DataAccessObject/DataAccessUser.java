/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccessObject;
import Controllers.Articles;
import Models.ModelLog;
import Models.ModelMedia;
import java.sql.*;
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import Models.ModelUser;
import Models.ModelType;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author migue
 */
public final class DataAccessUser {
    //Initialize as null
    DatabaseConnection databaseConnection = null;
    //Construtor
    public DataAccessUser()throws SQLException {
        //Connect to Database
        databaseConnection = new DatabaseConnection();
    }
    //Create User
    public void create(ModelUser user) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        try {
            DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
            String birthday = df.format(user.getUserBirthday());
            preparedstmt = connection.prepareStatement("call projectarticles.userInsert(?, ?, ?, ?, ?, ?, ?)");
            preparedstmt.setString(1, user.getUserId());
            preparedstmt.setString(2, user.getUserName());
            preparedstmt.setString(3, birthday);
            preparedstmt.setString(4, user.getUserEmail());
            preparedstmt.setString(5, user.getUserPassword());
            preparedstmt.setString(6, user.getType().getTypeId());
            preparedstmt.setString(7, user.getMedia().getMediaId());
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("users", "Error Creating User "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Creating User ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Update User
    public void updateById(ModelUser user) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        try {
            DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
            String birthday = df.format(user.getUserBirthday());
            preparedstmt = connection.prepareStatement("call projectarticles.userUpdate(?, ?, ?, ?, ?, ?)");
            preparedstmt.setString(1, user.getUserId());
            preparedstmt.setString(2, user.getUserName());
            preparedstmt.setString(3, birthday);
            preparedstmt.setString(4, user.getUserEmail());
            preparedstmt.setString(5, user.getUserPassword());
            preparedstmt.setString(6, user.getType().getTypeId());
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("users", "Error Updating User "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Updating User ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Delete User
    public void deleteById(String id)throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        ModelUser user = new ModelUser();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.userDelete(?)");
            preparedstmt.setString(1, id);
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("users", "Error Deleting User "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Deleting User ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Activate User
    public void activateById(String id)throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        ModelUser user = new ModelUser();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.userActivate(?)");
            preparedstmt.setString(1, id);
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("users", "Error Deleting User "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Deleting User ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Find by Id
    public ModelUser findById(String id) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        ModelUser user = new ModelUser();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.userFindById(?)");
            preparedstmt.setString(1, id);
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                user.setUserId(rs.getString("userId"));
                user.setUserName(rs.getString("userName"));
                user.setUserBirthday(rs.getDate("userBirthday"));
                user.setUserEmail(rs.getString("userEmail"));
                user.setUserPassword(rs.getString("userPassword"));
                user.setUserStatus(rs.getInt("userStatus"));
                ModelType type = new ModelType();
                type=type.findById(rs.getString("fk_typeId"));
                user.setType(type);
                ModelMedia media = new ModelMedia();
                media=media.findById(rs.getString("fk_mediaId"));
                user.setMedia(media);
                user.setCreated_at(rs.getTimestamp("created_at"));
                user.setUpdated_at(rs.getTimestamp("updated_at"));
            }
        } catch (SQLException ex) {
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("users", "Error Finding User "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Finding User ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return user;
    } 
     //Find by Id
    public ModelUser authenticateByEmail(String email) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        ModelUser user = new ModelUser();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.userAuthenticate(?)");
            preparedstmt.setString(1, email);
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                user.setUserId(rs.getString("userId"));
                user.setUserName(rs.getString("userName"));
                user.setUserBirthday(rs.getDate("userBirthday"));
                user.setUserEmail(rs.getString("userEmail"));
                user.setUserPassword(rs.getString("userPassword"));
                user.setUserStatus(rs.getInt("userStatus"));
                ModelType type = new ModelType();
                type=type.findById(rs.getString("fk_typeId"));
                user.setType(type);
                ModelMedia media = new ModelMedia();
                media=media.findById(rs.getString("fk_mediaId"));
                user.setMedia(media);
                user.setCreated_at(rs.getTimestamp("created_at"));
                user.setUpdated_at(rs.getTimestamp("updated_at"));
            }
        } catch (SQLException ex) {
            ModelLog log = new ModelLog("users", "Error Authenticating User "+ex, null, null); //ADICIONAR O ID DO UTILIZADOR AUTENTICADO
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Authenticating User ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return user;
    }        
    //List Active
    public List<ModelUser> listActive() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;

        List<ModelUser> lstUsers = new ArrayList<>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.userListActive()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                ModelUser user = new ModelUser();
                user.setUserId(rs.getString("userId"));
                user.setUserName(rs.getString("userName"));
                user.setUserBirthday(rs.getDate("userBirthday"));
                user.setUserEmail(rs.getString("userEmail"));
                user.setUserPassword(rs.getString("userPassword"));
                user.setUserStatus(rs.getInt("userStatus"));
                ModelType type = new ModelType();
                type=type.findById(rs.getString("fk_typeId"));
                user.setType(type);
                ModelMedia media = new ModelMedia();
                media=media.findById(rs.getString("fk_mediaId"));
                user.setMedia(media);
                user.setCreated_at(rs.getTimestamp("created_at"));
                user.setUpdated_at(rs.getTimestamp("updated_at"));
                lstUsers.add(user);
            }
        } catch (SQLException ex) {
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("users", "Error Listing User "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing User ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return lstUsers;
    }
    //List Inactive
    public List<ModelUser> listInactive() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;

        List<ModelUser> lstUsers = new ArrayList<>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.userListInactive()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                ModelUser user = new ModelUser();
                user.setUserId(rs.getString("userId"));
                user.setUserName(rs.getString("userName"));
                user.setUserBirthday(rs.getDate("userBirthday"));
                user.setUserEmail(rs.getString("userEmail"));
                user.setUserPassword(rs.getString("userPassword"));
                user.setUserStatus(rs.getInt("userStatus"));
                ModelType type = new ModelType();
                type=type.findById(rs.getString("fk_typeId"));
                user.setType(type);
                ModelMedia media = new ModelMedia();
                media=media.findById(rs.getString("fk_mediaId"));
                user.setMedia(media);
                user.setCreated_at(rs.getTimestamp("created_at"));
                user.setUpdated_at(rs.getTimestamp("updated_at"));
                lstUsers.add(user);
            }
        } catch (SQLException ex) {
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("users", "Error Listing User "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing User ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return lstUsers;
    }
}
