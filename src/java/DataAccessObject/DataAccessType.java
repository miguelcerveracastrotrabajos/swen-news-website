/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccessObject;
import Controllers.Articles;
import Models.ModelLog;
import java.sql.*;
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import Models.ModelType;
import Models.ModelUser;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author migue
 */
public final class DataAccessType {
    //Initialize as null
    DatabaseConnection databaseConnection = null;
    //Construtor
    public DataAccessType()throws SQLException {
        //Connect to Database
        databaseConnection = new DatabaseConnection();
    }
    //Create Type
    public void create(ModelType type) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        System.out.println(type.getTypeId()+" "+type.getTypeDesignation()+" "+type.getTypeDescription());
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.typeInsert(?, ?, ?)");
            preparedstmt.setString(1, type.getTypeId());
            preparedstmt.setString(2, type.getTypeDesignation());
            preparedstmt.setString(3, type.getTypeDescription());
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("types", "Error Creating Type "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Creating Type ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Update Type
    public void updateById(ModelType type) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.typeUpdate(?, ?, ?)");
            preparedstmt.setString(1, type.getTypeId());
            preparedstmt.setString(2, type.getTypeDesignation());
            preparedstmt.setString(3, type.getTypeDescription());
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("types", "Error Updating Type "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Updating Type ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Delete Type
    public void deleteById(String id)throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        ModelType type = new ModelType();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.typeDelete(?)");
            preparedstmt.setString(1, id);
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("types", "Error Deleting Type "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Deleting Type ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Activate Type
    public void activateById(String id)throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        ModelType type = new ModelType();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.typeActivate(?)");
            preparedstmt.setString(1, id);
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("types", "Error Activating Type "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Activating Type ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Find by Id
    public ModelType findById(String id) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        ModelType type = new ModelType();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.typeFindById(?)");
            preparedstmt.setString(1, id);
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                type.setTypeId(rs.getString("typeId"));
                type.setTypeDesignation(rs.getString("typeDesignation"));
                type.setTypeDescription(rs.getString("typeDescription"));
                type.setTypeStatus(rs.getInt("typeStatus"));
                type.setCreated_at(rs.getTimestamp("created_at"));
                type.setUpdated_at(rs.getTimestamp("updated_at"));
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("types", "Error Finding Type "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Finding Type ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return type;
    }           
    //List Active
    public List<ModelType> listActive() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;

        List<ModelType> lstTypes = new ArrayList<>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.typeListActive()");
            rs = preparedstmt.executeQuery();

            while (rs.next()) {
                ModelType type = new ModelType();
                type.setTypeId(rs.getString("typeId"));
                type.setTypeDesignation(rs.getString("typeDesignation"));
                type.setTypeDescription(rs.getString("typeDescription"));
                type.setTypeStatus(rs.getInt("typeStatus"));
                type.setCreated_at(rs.getTimestamp("created_at"));
                type.setUpdated_at(rs.getTimestamp("updated_at"));
                lstTypes.add(type);
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("types", "Error Listing Type "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Type ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return lstTypes;
    }
    //List Inactive
    public List<ModelType> listInactive() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;

        List<ModelType> lstTypes = new ArrayList<>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.typeListInactive()");
            rs = preparedstmt.executeQuery();

            while (rs.next()) {
                ModelType type = new ModelType();
                type.setTypeId(rs.getString("typeId"));
                type.setTypeDesignation(rs.getString("typeDesignation"));
                type.setTypeDescription(rs.getString("typeDescription"));
                type.setTypeStatus(rs.getInt("typeStatus"));
                type.setCreated_at(rs.getTimestamp("created_at"));
                type.setUpdated_at(rs.getTimestamp("updated_at"));
                lstTypes.add(type);
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("types", "Error Listing Type "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Type ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return lstTypes;
    }
}
