/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccessObject;
import Controllers.Articles;
import Models.ModelLog;
import java.sql.*;
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import Models.ModelMedia;
import Models.ModelUser;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author migue
 */
public final class DataAccessMedia {
    //Initialize as null
    DatabaseConnection databaseConnection = null;
    //Construtor
    public DataAccessMedia()throws SQLException {
        //Connect to Database
        databaseConnection = new DatabaseConnection();
    }
    //Create Media
    public void create(ModelMedia media) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.mediaInsert(?, ?, ?)");
            preparedstmt.setString(1, media.getMediaId());
            preparedstmt.setString(2, media.getMediaName());
            preparedstmt.setString(3, media.getMediaType());
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("medias", "Error Creating Media "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Creating Media ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Update Media
    public void updateById(ModelMedia media) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.mediaUpdate(?, ?, ?)");
            preparedstmt.setString(1, media.getMediaId());
            preparedstmt.setString(2, media.getMediaName());
            preparedstmt.setString(3, media.getMediaType());
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("medias", "Error Updating Media "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Updating Media ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Delete Media
    public void deleteById(String id)throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        ModelMedia media = new ModelMedia();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.mediaDelete(?)");
            preparedstmt.setString(1, id);
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("medias", "Error Deleting Media "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Deleting Media ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Find by Id
    public ModelMedia findById(String id) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        ModelMedia media = new ModelMedia();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.mediaFindById(?)");
            preparedstmt.setString(1, id);
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                media.setMediaId(rs.getString("mediaId"));
                media.setMediaName(rs.getString("mediaName"));
                media.setMediaType(rs.getString("mediaType"));
                media.setMediaStatus(rs.getInt("mediaStatus"));
                media.setCreated_at(rs.getTimestamp("created_at"));
                media.setUpdated_at(rs.getTimestamp("updated_at"));
            }
        } catch (SQLException ex) {
             //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("medias", "Error Finding Media "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Finding Media ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return media;
    }           
    //List Active
    public List<ModelMedia> listActive() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;

        List<ModelMedia> lstMedia = new ArrayList<>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.mediaListActive()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                ModelMedia media = new ModelMedia();
                media.setMediaId(rs.getString("mediaId"));
                media.setMediaName(rs.getString("mediaName"));
                media.setMediaType(rs.getString("mediaType"));
                media.setMediaStatus(rs.getInt("mediaStatus"));
                media.setCreated_at(rs.getTimestamp("created_at"));
                media.setUpdated_at(rs.getTimestamp("updated_at"));
            }
        } catch (SQLException ex) {
             //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("medias", "Error Listing Media "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Media ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return lstMedia;
    }
    
}
