/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccessObject;
import Controllers.Articles;
import java.sql.*;
import java.sql.Connection;
import java.sql.SQLException;
import Models.ModelLog;
import Models.ModelUser;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author migue
 */
public class DataAccessLog {
    //Initialize as null
    DatabaseConnection databaseConnection = null;
    //Construtor
    public DataAccessLog()throws SQLException {
        //Connect to Database
        databaseConnection = new DatabaseConnection();
    }
    //Create Media
    public void create(ModelLog log) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.logInsert(?, ?, ?, ?)");
            preparedstmt.setString(1, log.getLogDesignation());
            preparedstmt.setString(2, log.getLogDescription());
            preparedstmt.setString(3, log.getLogElement());
            preparedstmt.setString(4, log.getLogUser().getUserId());
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            this.create(new ModelLog("logs", "Error Creating Log "+ex, null, user));
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Creating Log ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //List View Logs
    public List<ModelLog> listViewLogs() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;

        List<ModelLog> lstLogs = new ArrayList<>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.logListView()");
            rs = preparedstmt.executeQuery();

            while (rs.next()) {
                ModelLog log = new ModelLog();
                log.setLogDesignation(rs.getString("logDesignation"));
                log.setLogDescription(rs.getString("logDescription"));
                ModelUser user = new ModelUser();
                log.setLogUser(user.findById(rs.getString("fk_userid")));
                log.setCreated_at(rs.getTimestamp("created_at"));
                log.setUpdated_at(rs.getTimestamp("updated_at"));
                lstLogs.add(log);
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("logs", "Error Listing Log "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Log ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return lstLogs;
    }
    //List Errors Logs
    public List<ModelLog> listErrorLogs() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;

        List<ModelLog> lstLogs = new ArrayList<>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.logListError()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                ModelLog log = new ModelLog();
                log.setLogDesignation(rs.getString("logDesignation"));
                log.setLogDescription(rs.getString("logDescription"));
                ModelUser user = new ModelUser();
                log.setLogUser(user.findById(rs.getString("fk_userid")));
                log.setCreated_at(rs.getTimestamp("created_at"));
                log.setUpdated_at(rs.getTimestamp("updated_at"));
                lstLogs.add(log);
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("logs", "Error Listing Log "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Log ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return lstLogs;
    }
    //List Action Logs
    public List<ModelLog> listActionLogs() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;

        List<ModelLog> lstLogs = new ArrayList<>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.logListAction()");
            rs = preparedstmt.executeQuery();

            while (rs.next()) {
                ModelLog log = new ModelLog();
                log.setLogDesignation(rs.getString("logDesignation"));
                log.setLogDescription(rs.getString("logDescription"));
                log.setLogElement(rs.getString("logElement"));
                ModelUser user = new ModelUser();
                log.setLogUser(user.findById(rs.getString("fk_userid")));
                log.setCreated_at(rs.getTimestamp("created_at"));
                log.setUpdated_at(rs.getTimestamp("updated_at"));
                lstLogs.add(log);
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("logs", "Error Listing Log "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Log ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return lstLogs;
    }
    //List Action Logs
    public List<Integer> getGraphsAndStats() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        List<Integer> counters = new ArrayList<Integer>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.logGraphsAndStats()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                counters.add(rs.getInt("a00"));
                counters.add(rs.getInt("a01"));
                counters.add(rs.getInt("a02"));
                counters.add(rs.getInt("a03"));
                counters.add(rs.getInt("a04"));
                counters.add(rs.getInt("a05"));
                counters.add(rs.getInt("a06"));
                counters.add(rs.getInt("a07"));
                counters.add(rs.getInt("a08"));
                counters.add(rs.getInt("a09"));
                counters.add(rs.getInt("a10"));
                counters.add(rs.getInt("a11"));
                counters.add(rs.getInt("a12"));
                counters.add(rs.getInt("a13"));
                counters.add(rs.getInt("a14"));
                counters.add(rs.getInt("a15"));
                counters.add(rs.getInt("a16"));
                counters.add(rs.getInt("a17"));
                counters.add(rs.getInt("a18"));
                counters.add(rs.getInt("a19"));
                counters.add(rs.getInt("a20"));
                counters.add(rs.getInt("a21"));
                counters.add(rs.getInt("a22"));
                counters.add(rs.getInt("a23"));
                counters.add(rs.getInt("a24"));
                counters.add(rs.getInt("a25"));
                counters.add(rs.getInt("a26"));
                counters.add(rs.getInt("a27"));
                counters.add(rs.getInt("a28"));
                counters.add(rs.getInt("a29"));
                counters.add(rs.getInt("a30"));
                counters.add(rs.getInt("a31"));
                counters.add(rs.getInt("a32"));
                counters.add(rs.getInt("a33"));
                counters.add(rs.getInt("a34"));
                counters.add(rs.getInt("a35"));
                counters.add(rs.getInt("a36"));
                counters.add(rs.getInt("a37"));
                counters.add(rs.getInt("a38"));
                counters.add(rs.getInt("a39"));
                counters.add(rs.getInt("a40"));
                counters.add(rs.getInt("a41"));
                counters.add(rs.getInt("a42"));
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("logs", "Error Listing Graphs and Stats "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Graphs and Stats ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return counters;
    }
    
    //Articles per user
    public List<String> getArticlesPerUser() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        List<String> info = new ArrayList<String>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.logArticlesPerUser()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                info.add(rs.getString("userName")+"_"+rs.getString("count"));
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("logs", "Error Listing Articles per User "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Articles per User ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return info;
    }
    //Users per Type
    public List<String> getUsersPerType() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        List<String> info = new ArrayList<String>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.logUsersPerType()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                info.add(rs.getString("typeDesignation")+"_"+rs.getString("count"));
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("logs", "Error Listing Users per Type "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Articles per User ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return info;
    }
    //Views per Article
    public List<String> getViewsPerArticle() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        List<String> info = new ArrayList<String>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.logViewsPerArticle()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                info.add(rs.getString("articleTitle")+"_"+rs.getString("count"));
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("logs", "Error Listing Views Per Article "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Articles per User ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return info;
    }
    //Views per User
    public List<String> getViewsPerUser() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        List<String> info = new ArrayList<String>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.logViewsPerUser()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                info.add(rs.getString("userName")+"_"+rs.getString("count"));
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("logs", "Error Listing Views Per User "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Views per User ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return info;
    }
    //Logs Per day
    public List<String> getLogsPerDay() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        List<String> info = new ArrayList<String>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.logLogsPerDay()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                info.add(rs.getString("created_at")+"_"+rs.getString("count"));
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("logs", "Error Listing Logs Per Day "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Logs per Day ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return info;
    }
    //Articles Per day
    public List<String> getArticlesPerDay() throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        List<String> info = new ArrayList<String>();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.logArticlesPerDay()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                info.add(rs.getString("created_at")+"_"+rs.getString("count"));
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("logs", "Error Listing Articles Per Day "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Listing Articles per Day ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return info;
    }
}
