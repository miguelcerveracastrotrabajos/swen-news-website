/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccessObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author migue
 */
public class DatabaseConnection {
    //Configuration Database Variables
        public static final String driver = "com.mysql.jdbc.Driver";
        public static final String database = "ProjectArticles";
        public static final String server = "jdbc:mysql://localhost:3306/";
        public static final String user = "root";
        public static final String password = "";
        public static final String encoding = "?useUnicode=true&characterEncoding=utf8generalci";
    //Methods
        //Connect to Database
        public static Connection connectDatabase(){
            Connection connect;
            try{
              //Driver
              Class.forName(driver);
              //Connection
              connect = DriverManager.getConnection(server+database,user,password);
              //Return Connection
              return connect;
            }catch (ClassNotFoundException|SQLException ex){
                //Show Errors from class
                throw new RuntimeException("Error Connecting to Database", ex);
            }
        }
        //Disconnect to Database
        public static void disconnectDatabase(Connection connect){
            try {
                //Close Desired Connection
                connect.close();
            } catch (SQLException ex) {
                //Show Errors from class
                throw new RuntimeException("Error Disconnecting", ex);
            }
        }
    //
}
