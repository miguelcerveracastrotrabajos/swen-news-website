/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccessObject;
import Controllers.Articles;
import java.sql.*;
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import Models.ModelArticle;
import Models.ModelLog;
import Models.ModelUser;
import Models.ModelMedia;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author migue
 */
public final class DataAccessArticle {
    //Initialize as null
    DatabaseConnection databaseConnection = null;
    //Construtor
    public DataAccessArticle()throws SQLException {
        //Connect to Database
        databaseConnection = new DatabaseConnection();
    }
    //Create Media
    public void create(ModelArticle article) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.articleInsert(?, ?, ?, ?, ?, ?)");
            preparedstmt.setString(1, article.getArticleId());
            preparedstmt.setString(2, article.getArticleTitle());
            preparedstmt.setString(3, article.getArticleSubtitle());
            preparedstmt.setString(4, article.getArticleBody());
            preparedstmt.setString(5, article.getAuthor().getUserId());
            preparedstmt.setString(6, article.getImage().getMediaId());
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("articles", "Error Creating Article "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Creating Article ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        
    }
    //Update Media
    public void updateById(ModelArticle article) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.articleUpdate(?, ?, ?, ?, ?)");
            preparedstmt.setString(1, article.getArticleId());
            preparedstmt.setString(2, article.getArticleTitle());
            preparedstmt.setString(3, article.getArticleSubtitle());
            preparedstmt.setString(4, article.getArticleBody());
            preparedstmt.setString(5, article.getAuthor().getUserId());
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("articles", "Error Updating Article "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Updating Article ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Delete Article
    public void deleteById(String id)throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        ModelArticle article = new ModelArticle();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.articleDelete(?)");
            preparedstmt.setString(1, id);
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("articles", "Error Deleting Article "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Finding Article ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Activate Article
    public void activateById(String id)throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        ModelArticle article = new ModelArticle();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.articleActivate(?)");
            preparedstmt.setString(1, id);
            rs = preparedstmt.executeQuery();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("articles", "Error Activating Article "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Activating Article ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
    }
    //Find by Id
    public ModelArticle findById(String id) throws SQLException{
        //Get connection
        Connection connection = DatabaseConnection.connectDatabase();
        PreparedStatement preparedstmt = null;
        ResultSet rs = null;
        ModelArticle article = new ModelArticle();
        try {
            preparedstmt = connection.prepareStatement("call projectarticles.articleFindById(?)");
            preparedstmt.setString(1, id);
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                article.setArticleId(rs.getString("articleId"));
                article.setArticleTitle(rs.getString("articleTitle"));
                article.setArticleSubtitle(rs.getString("articleSubtitle"));
                article.setArticleBody(rs.getString("articleBody"));
                article.setArticleStatus(rs.getInt("articleStatus"));
                
                ModelUser user = new ModelUser();
                user=user.findById(rs.getString("fk_userId"));
                article.setAuthor(user);
                
                ModelMedia media = new ModelMedia();
                media=media.findById(rs.getString("fk_mediaId"));
                article.setImage(media);
                
                article.setCreated_at(rs.getTimestamp("created_at"));
                article.setUpdated_at(rs.getTimestamp("updated_at"));
            }
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("articles", "ParseException: Error Finding Article "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Finding Article ", ex);
        }
        rs.close();
        preparedstmt.close();
        connection.close();
        return article;
    }           
    //List Active
    public List<ModelArticle> listActive(){
        List<ModelArticle> lstArticles = new ArrayList<>();
        try {
            //Get connection
            Connection connection = DatabaseConnection.connectDatabase();
            PreparedStatement preparedstmt = null;
            ResultSet rs = null;
            preparedstmt = connection.prepareStatement("call projectarticles.articleListActive()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                ModelArticle article = new ModelArticle();
                article.setArticleId(rs.getString("articleId"));
                article.setArticleTitle(rs.getString("articleTitle"));
                article.setArticleSubtitle(rs.getString("articleSubtitle"));
                article.setArticleBody(rs.getString("articleBody"));
                article.setArticleStatus(rs.getInt("articleStatus"));
                
                ModelUser user = new ModelUser();
                user=user.findById(rs.getString("fk_userId"));
                article.setAuthor(user);
                
                ModelMedia media = new ModelMedia();
                media=media.findById(rs.getString("fk_mediaId"));
                article.setImage(media);
                article.setArticleViews(rs.getInt("Views"));
                article.setCreated_at(rs.getTimestamp("created_at"));
                article.setUpdated_at(rs.getTimestamp("updated_at"));
                lstArticles.add(article);
            }
            rs.close();
            preparedstmt.close();
            connection.close();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("articles", "ParseException: Error Getting the Types "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Getting the Types "+ex);
        }
        
        return lstArticles;
    }
     //List Active
    public List<ModelArticle> listInactive(){
        List<ModelArticle> lstArticles = new ArrayList<>();
        try {
            //Get connection
            Connection connection = DatabaseConnection.connectDatabase();
            PreparedStatement preparedstmt = null;
            ResultSet rs = null;
            preparedstmt = connection.prepareStatement("call projectarticles.articleListInactive()");
            rs = preparedstmt.executeQuery();
            while (rs.next()) {
                ModelArticle article = new ModelArticle();
                article.setArticleId(rs.getString("articleId"));
                article.setArticleTitle(rs.getString("articleTitle"));
                article.setArticleSubtitle(rs.getString("articleSubtitle"));
                article.setArticleBody(rs.getString("articleBody"));
                article.setArticleStatus(rs.getInt("articleStatus"));
                
                ModelUser user = new ModelUser();
                user=user.findById(rs.getString("fk_userId"));
                article.setAuthor(user);
                
                ModelMedia media = new ModelMedia();
                media=media.findById(rs.getString("fk_mediaId"));
                article.setImage(media);
                
                article.setCreated_at(rs.getTimestamp("created_at"));
                article.setUpdated_at(rs.getTimestamp("updated_at"));
                lstArticles.add(article);
            }
            rs.close();
            preparedstmt.close();
            connection.close();
        } catch (SQLException ex) {
            //CREATE LOG REGISTING THE ACTION
            ModelUser user = new ModelUser();
            user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            ModelLog log = new ModelLog("articles", "ParseException: Error Getting the Types "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Getting the Types "+ex);
        }
        
        return lstArticles;
    }
}
