/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.ModelAuthentication;
import Models.ModelHashing;
import Models.ModelLog;
import Models.ModelMedia;
import Models.ModelReturnMessages;
import Models.ModelType;
import Models.ModelUser;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "CreateUser", urlPatterns = {"/users_create"})
@MultipartConfig
public class CreateUser extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser authU = new ModelUser();
            try {
                authU = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        try {
            // <editor-fold defaultstate="collapsed" desc="Load view to create Users">
            //SEND VARIABLES
            request.setAttribute("contentPage", "createuser");
            //CREATE LOG REGISTING THE ACTION
            ModelLog log = new ModelLog("users", "View: Access view Create User", null, authU);
            log.create();
            //CHOOSE FILE TO SEND IT TO
            request.getRequestDispatcher("layout.jsp").forward(request,response);
            // </editor-fold>
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("users", "SQLException: Error Creating the user "+ex, null, authU);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Creating the User "+ex);
            //</editor-fold>
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            
            ModelUser authU = new ModelUser();
            try {
                authU = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        //</editor-fold>
        try {
            // <editor-fold defaultstate="collapsed" desc="Setting the Correct Encoding and Respose Type">
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Initializing Class">
            ModelType type = new ModelType();
            ModelUser user = new ModelUser();
            ModelMedia media = new ModelMedia();
            ModelHashing hash = new ModelHashing();
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Creating a single User">
            user.setUserName(request.getParameter("username"));
            user.setUserPassword(hash.hashPassword(request.getParameter("userpassword")));
            user.setUserEmail(request.getParameter("useremail"));
            type.setTypeId(request.getParameter("fktype"));
            DateFormat  format = new SimpleDateFormat("yyyy-mm-dd");
            java.util.Date utilStartDate = format.parse(request.getParameter("userbirthday"));
            java.sql.Date date = new java.sql.Date(utilStartDate.getTime());  
            user.setUserBirthday((java.sql.Date) date);
            user.setType(type);
            media.setMediaId(media.create(authU, request, getServletContext().getRealPath("/")));
            user.setMedia(media);
            System.out.println(authU.getUserId());
            String userId=user.create(authU);
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Returning Message">
            Gson gson = new Gson();
            String message = "Successfully created a user with the id of "+userId;
            ModelReturnMessages returnElement = new ModelReturnMessages(message, new ArrayList<>());
            String jsonData = gson.toJson(returnElement);
            PrintWriter out = response.getWriter();
            try {
                out.println(jsonData);
            } finally {
                out.close();
            }
            //</editor-fold>
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("users", "SQLException: Error Creating the user "+ex, null, authU); 
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Creating the User "+ex);
            //</editor-fold>
        } catch (ParseException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("users", "ParseException: Error Creating the user "+ex, null, authU);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Creating the User "+ex);
            //</editor-fold>
        }
    } 
}
