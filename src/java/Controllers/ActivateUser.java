/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.ModelAuthentication;
import Models.ModelLog;
import Models.ModelReturnMessages;
import Models.ModelUser;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "ActivateUser", urlPatterns = {"/users_activate"},
        initParams = {
                @WebInitParam(name = "id",value="*")
            }
        )
@MultipartConfig
public class ActivateUser extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser authU = new ModelUser();
            try {
                authU = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Setting the Correct Encoding and Respose User">
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Initializing Class and Variables">
        ModelUser user = new ModelUser();
        String message="";
        String userid=request.getParameter("id");
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Activating a single User">
        try {
            user.activateById(userid, authU);
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("types", "ParseException: Error Activating the user "+ex, userid, authU);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Activating the Users "+ex);
            //</editor-fold>
        }
        message = "Successfully activated the user with the id of "+userid;       
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Returning Message">
        Gson gson = new Gson();
        ModelReturnMessages returnElement = new ModelReturnMessages(message, new ArrayList<>());
        String jsonData = gson.toJson(returnElement);
        PrintWriter out = response.getWriter();
        try {
            out.println(jsonData);
        } finally {
            out.close();
        }
        //</editor-fold>
    }
}
