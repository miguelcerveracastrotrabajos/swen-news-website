/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;
import Models.ModelAuthentication;
import Models.ModelType;
import Models.ModelLog;
import Models.ModelUser;
import Models.ModelReturnMessages;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "CreateType", urlPatterns = {"/types_create"})
@MultipartConfig
public class CreateType extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = new ModelUser();
            try {
                user = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        try {
            // <editor-fold defaultstate="collapsed" desc="Load view to create Types">
            request.setAttribute("contentPage", "createtype");
            //CREATE LOG REGISTING THE ACTION
            ModelLog log = new ModelLog("types", "View: Access view Create Type", null, user);
            log.create();
            //CHOOSE FILE TO SEND IT TO
            request.getRequestDispatcher("layout.jsp").forward(request,response);
            // </editor-fold>
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("types", "SQLException: Error Creating the type "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Creating the type "+ex);
            //</editor-fold>
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = new ModelUser();
            try {
                user = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        try {
            // <editor-fold defaultstate="collapsed" desc="Setting the Correct Encoding and Respose Type">
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Initializing Class">
            ModelType type = new ModelType();
            //</editor-fold>
            System.out.println(request.getParameter("typedesignation")+" "+request.getParameter("typedescription"));
            /// <editor-fold defaultstate="collapsed" desc="Creating a single Type">
            type.setTypeDesignation(request.getParameter("typedesignation"));
            type.setTypeDescription(request.getParameter("typedescription"));
            String typeId=type.create(user);
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Returning Message">
            Gson gson = new Gson();
            String message = "Successfully created a user type with the id of "+typeId;
            ModelReturnMessages returnElement = new ModelReturnMessages(message, new ArrayList<>());
            String jsonData = gson.toJson(returnElement);
            PrintWriter out = response.getWriter();
            try {
                out.println(jsonData);
            } finally {
                out.close();
            }
            //</editor-fold>
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("types", "SQLException: Error Creating the type "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Creating the type "+ex);
            //</editor-fold>
        }
    } 
}
