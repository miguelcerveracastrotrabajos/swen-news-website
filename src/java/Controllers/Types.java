/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;
import Models.ModelAuthentication;
import Models.ModelLog;
import Models.ModelReturnMessages;
import Models.ModelType;
import Models.ModelUser;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "Types", urlPatterns = {"/types"},
        initParams = {
                @WebInitParam(name = "id",value="*")
            }
        )
@MultipartConfig
public class Types extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = new ModelUser();
            try {
                user = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        try {
            // <editor-fold defaultstate="collapsed" desc="Initializing Class">
                ModelType type = new ModelType();
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Load all Types">
            if(request.getParameter("id")==null){
                //LOAD ALL ACTIVE TYPES                
                List<ModelType> lstactive=type.listActive();
                List<ModelType> lstinactive=type.listInactive();
                //CREATE LOG REGISTING THE ACTION
                ModelLog log = new ModelLog("types", "View: Access Active Types", null, user);
                log.create();
                //SEND VARIABLES
                request.setAttribute("contentPage", "types");
                request.setAttribute("lstactive", lstactive);
                request.setAttribute("lstinactive", lstinactive);
                //CHOOSE FILE TO SEND IT TO
                request.getRequestDispatcher("layout.jsp").forward(request,response);
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Load a single User">
            else{
                //LOAD ALL ACTIVE TYPES
                type = type.findById(request.getParameter("id"));
                //CREATE LOG REGISTING THE ACTION
                ModelLog log = new ModelLog("types", "View: Access view Type", request.getParameter("id"), user);
                log.create();
                //SEND VARIABLES
                request.setAttribute("contentPage", "article");
                request.setAttribute("type", type);
                //CHOOSE FILE TO SEND IT TO
                request.getRequestDispatcher("layout.jsp").forward(request,response);
            }
            //</editor-fold>
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("types", "ParseException: Error Creating the type "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Getting the Types "+ex);
            //</editor-fold>
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = new ModelUser();
            try {
                user = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Setting the Correct Encoding and Respose Type">
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Initializing Class and Variables">
        ModelType type = new ModelType();
        String message="";
        String typeId=request.getParameter("id");
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Updating a single Type">
        if(request.getParameter("typedesignation")!=null){
            //ADD PARAMETERS TO OBJECT
            type.setTypeId(typeId);
            //ADD PARAMETERS TO OBJECT
            type.setTypeDesignation(request.getParameter("typedesignation"));
            type.setTypeDescription(request.getParameter("typedescription"));
            try {
                type.updateById(user);
            } catch (SQLException ex) {
               // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
                ModelLog log = new ModelLog("types", "ParseException: Error Updating the type "+ex, null, user);
                try {
                    log.create();
                } catch (SQLException ex1) {
                    Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
                }
                throw new RuntimeException("Error Getting the Types "+ex);
                //</editor-fold>
            }
            message = "Successfully updating the type with the id of "+typeId;
        }
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Deleting a single Type">
        else{         
            //ADD PARAMETERS TO OBJECT
            type.setTypeId(typeId);
            try {
                type.deleteById(user);
            } catch (SQLException ex) {
                // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
                ModelLog log = new ModelLog("types", "ParseException: Error Updating the type "+ex, null, user);
                try {
                    log.create();
                } catch (SQLException ex1) {
                    Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
                }
                throw new RuntimeException("Error deleting the Types "+ex);
                //</editor-fold>
            }
            message = "Successfully deleting the type with the id of "+typeId;               
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Returning Message">
        Gson gson = new Gson();
        ModelReturnMessages returnElement = new ModelReturnMessages(message, new ArrayList<>());
        String jsonData = gson.toJson(returnElement);
        PrintWriter out = response.getWriter();
        try {
            out.println(jsonData);
        } finally {
            out.close();
        }
        //</editor-fold>
    }
}
