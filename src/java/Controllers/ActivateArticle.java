/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.ModelArticle;
import Models.ModelAuthentication;
import Models.ModelLog;
import Models.ModelReturnMessages;
import Models.ModelUser;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "ActivateArticle", urlPatterns = {"/articles_activate"},
        initParams = {
                @WebInitParam(name = "id",value="*")
            }
        )
@MultipartConfig
public class ActivateArticle extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = new ModelUser();
            try {
                user = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Setting the Correct Encoding and Respose Type">
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Initializing Class and Variables">
            ModelArticle article = new ModelArticle();
            String message="";
            String articleId=request.getParameter("id");
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Activating a single Article">
         //INITIALIZE OBJECTS NEEDED
            article.setAuthor(user);
            //ADD PARAMETERS TO OBJECT
            article.setArticleId(articleId);
            try {
                article.activateById(user);
            } catch (SQLException ex) {
                // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
                ModelLog log = new ModelLog("articles", "SQLException: Error Activating the Article "+ex, null, user);
                try {
                    log.create();
                } catch (SQLException ex1) {
                    Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
                }
                throw new RuntimeException("Error Activating the Article ", ex);
                //</editor-fold>
            }
            message = "Successfully activating an article with the id of "+articleId;     
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Returning Message">
        Gson gson = new Gson();
        ModelReturnMessages returnElement = new ModelReturnMessages(message, new ArrayList<>());
        String jsonData = gson.toJson(returnElement);
        PrintWriter out = response.getWriter();
        try {
            out.println(jsonData);
        } finally {
            out.close();
        }
        //</editor-fold>
    }
}
