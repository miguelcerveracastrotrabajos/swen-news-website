/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.ModelAuthentication;
import Models.ModelLog;
import Models.ModelReturnMessages;
import Models.ModelType;
import Models.ModelUser;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "ActivateType", urlPatterns = {"/types_activate"},
        initParams = {
                @WebInitParam(name = "id",value="*")
            }
        )
@MultipartConfig
public class ActivateType extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = new ModelUser();
            try {
                user = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Setting the Correct Encoding and Respose Type">
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Initializing Class and Variables">
        ModelType type = new ModelType();
        String message="";
        String typeId=request.getParameter("id");
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Activating a single Type">
        //ADD PARAMETERS TO OBJECT
        type.setTypeId(typeId);
        try {
            type.activateById(user);
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("types", "ParseException: Error Activating the type "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Activating the Types "+ex);
            //</editor-fold>
        }
        message = "Successfully activated the type with the id of "+typeId;       
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Returning Message">
        Gson gson = new Gson();
        ModelReturnMessages returnElement = new ModelReturnMessages(message, new ArrayList<>());
        String jsonData = gson.toJson(returnElement);
        PrintWriter out = response.getWriter();
        try {
            out.println(jsonData);
        } finally {
            out.close();
        }
        //</editor-fold>
    }
}
