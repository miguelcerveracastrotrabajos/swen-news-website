/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.ModelAuthentication;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "UserLogin", urlPatterns = {"/login_user"})
public class MainLogin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {
            // <editor-fold defaultstate="collapsed" desc="Setting the Correct Encoding and Respose Type">
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Initializing Variables">
                String message="";
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Authenticating a User">
                ModelAuthentication auth = new ModelAuthentication();
                auth.authenticationNormalUser(request, response);
            //</editor-fold>
            
        } catch (SQLException ex) {
            Logger.getLogger(AuthenticateUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
     protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
         // <editor-fold defaultstate="collapsed" desc="Setting the Correct Encoding and Respose Type">
         response.setContentType("application/json");
         response.setCharacterEncoding("utf-8");
         //</editor-fold>
         // <editor-fold defaultstate="collapsed" desc="Initializing Variables">
         String message="";
         //</editor-fold>
         // <editor-fold defaultstate="collapsed" desc="Authenticating a User">
         ModelAuthentication auth = new ModelAuthentication();
         auth.unauthenticateNormalUser(request, response);
         //</editor-fold>
        
    }

}
