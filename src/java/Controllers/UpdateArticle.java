/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.ModelArticle;
import Models.ModelAuthentication;
import Models.ModelLog;
import Models.ModelUser;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "UpdateArticle", urlPatterns = {"/articles_update"},
        initParams = {
                @WebInitParam(name = "id",value="*")
            }
        )
public class UpdateArticle extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = new ModelUser();
            try {
                user = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        try {
            // <editor-fold defaultstate="collapsed" desc="Load view to update Articles">
            ModelArticle article = new ModelArticle();
            //SEND VARIABLES
            request.setAttribute("contentPage", "updatearticle");
            request.setAttribute("article", article.findById(request.getParameter("id")));
            //CREATE LOG REGISTING THE ACTION
            ModelLog log = new ModelLog("articles", "View: Access view Update Article", null, user);
            log.create();
            //CHOOSE FILE TO SEND IT TO
            request.getRequestDispatcher("layout.jsp").forward(request,response);
            // </editor-fold>
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("articles", "SQLException: Error Updating the Article "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Updating the Article ", ex);
            //</editor-fold>
        }
        
    }
}
