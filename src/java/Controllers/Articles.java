/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.ModelArticle;
import Models.ModelAuthentication;
import Models.ModelLog;
import Models.ModelMedia;
import Models.ModelReturnMessages;
import Models.ModelUser;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "Articles", urlPatterns = {"/articles"},
        initParams = {
                @WebInitParam(name = "id",value="*")
            }
        )
@MultipartConfig
public class Articles extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = new ModelUser();
            try {
                user = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        try {
            // <editor-fold defaultstate="collapsed" desc="Initializing Class">
            ModelArticle article = new ModelArticle();
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Load all Articles">
            if(request.getParameter("id")==null){
                List<ModelArticle> lstactive=article.listActive();
                List<ModelArticle> lstinactive=article.listInactive();
                //CREATE LOG REGISTING THE ACTION
                ModelLog log = new ModelLog("articles", "View: Access Active Articles", null, user);
                log.create();
                //SEND VARIABLES
                request.setAttribute("contentPage", "articles");
                request.setAttribute("lstactive", lstactive);
                request.setAttribute("lstinactive", lstinactive);
                //CHOOSE FILE TO SEND IT TO
                request.getRequestDispatcher("layout.jsp").forward(request,response);
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Load a single Article">
            else{
                article = article.findById(request.getParameter("id"));
                //CREATE LOG REGISTING THE ACTION
                ModelLog log = new ModelLog("articles", "View: Access view Article", request.getParameter("id"), user);
                log.create();
                //SEND VARIABLES
                request.setAttribute("contentPage", "article");
                request.setAttribute("article", article);
                //CHOOSE FILE TO SEND IT TO
                request.getRequestDispatcher("layout.jsp").forward(request,response);
            }
            //</editor-fold>
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("articles", "SQLException: Error Getting the Articles "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Getting the Articles ", ex);
            //</editor-fold>
        }
        
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {
            // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = auth.setContent(request, response);
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Setting the Correct Encoding and Respose Type">
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Initializing Class and Variables">
            ModelArticle article = new ModelArticle();
            String message="";
            String articleId=request.getParameter("id");
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Updating a single Article">
            if(request.getParameter("articlebody")!=null){
                ModelMedia media = new ModelMedia();
                
                try {
                    //ADD PARAMETERS TO OBJECT
                    article=article.findById(articleId);
                    article.setArticleId(articleId);
                    article.setArticleBody(request.getParameter("articlebody"));
                    article.setArticleSubtitle(request.getParameter("articlesubtitle"));
                    article.setArticleTitle(request.getParameter("articletitle"));
                    article.setAuthor(user);
                    media.updateById(article.getImage().getMediaId(), user, request ,getServletContext().getRealPath("/"));
                    article.updateById(user);
                    
                } catch (SQLException ex) {
                    // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
                    ModelLog log = new ModelLog("articles", "SQLException: Error Updating the Article "+ex, null, user);
                    try {
                        log.create();
                    } catch (SQLException ex1) {
                        Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    throw new RuntimeException("Error Updating the Article ", ex);
                    //</editor-fold>
                }
                message = "Successfully updating an article with the id of "+articleId;
            }
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Deleting a single Article">
            else{
                //INITIALIZE OBJECTS NEEDED
                article.setAuthor(user);
                //ADD PARAMETERS TO OBJECT
                article.setArticleId(articleId);
                try {
                    article.deleteById(user);
                } catch (SQLException ex) {
                    // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
                    user.setUserId("2a2f432c-5925-11e9-bcd0-086266b4ab53");
                    ModelLog log = new ModelLog("articles", "SQLException: Error Deleting the Article "+ex, null, user);
                    try {
                        log.create();
                    } catch (SQLException ex1) {
                        Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    throw new RuntimeException("Error Deleting the Article ", ex);
                    //</editor-fold>
                }
                message = "Successfully deleting an article with the id of "+articleId;
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Returning Message">
            Gson gson = new Gson();
            ModelReturnMessages returnElement = new ModelReturnMessages(message, new ArrayList<>());
            String jsonData = gson.toJson(returnElement);
            PrintWriter out = response.getWriter();
            try {
                out.println(jsonData);
            } finally {
                out.close();
            }
            //</editor-fold>
        }   catch (SQLException ex) {
            Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        
    }
}
