/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;
import Models.ModelArticle;
import Models.ModelAuthentication;
import Models.ModelLog;
import Models.ModelMedia;
import Models.ModelUser;
import Models.ModelReturnMessages;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "CreateArticle", urlPatterns = {"/articles_create"})
@MultipartConfig
public class CreateArticle extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = new ModelUser();
            try {
                user = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        try {
            // <editor-fold defaultstate="collapsed" desc="Load view to create Articles">
            //SEND VARIABLES
            request.setAttribute("contentPage", "createarticle");
            //CREATE LOG REGISTING THE ACTION
            ModelLog log = new ModelLog("articles", "View: Access view Create Article", null, user);
            log.create();
            //CHOOSE FILE TO SEND IT TO
            request.getRequestDispatcher("layout.jsp").forward(request,response);
            // </editor-fold>
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("articles", "SQLException: Error Creating the Article "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Creating the Article ", ex);
            //</editor-fold>
        }
        
    }
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = new ModelUser();
            try {
                user = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        try {
            // <editor-fold defaultstate="collapsed" desc="Setting the Correct Encoding and Respose Type">
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Initializing Class">
            ModelArticle article = new ModelArticle();
            ModelMedia media = new ModelMedia();
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Creating a single Article">
            article.setArticleBody(request.getParameter("articlebody"));
            article.setArticleSubtitle(request.getParameter("articlesubtitle"));
            article.setArticleTitle(request.getParameter("articletitle"));
            article.setAuthor(user);
            media.setMediaId(media.create(user, request ,getServletContext().getRealPath("/")));
            article.setImage(media);
            String articleId=article.create(user);
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Returning Message">
            Gson gson = new Gson();
            String message = "Successfully created an article with the id of "+articleId;
            ModelReturnMessages returnElement = new ModelReturnMessages(message, new ArrayList<>());
            String jsonData = gson.toJson(returnElement);
            PrintWriter out = response.getWriter();
            try {
                out.println(jsonData);
            } finally {
                out.close();
            }
            //</editor-fold>
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("articles", "SQLException: Error Creating the Article "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
           throw new RuntimeException("Error Creating the Article "+ex);
           //</editor-fold>
        }
    } 
}
