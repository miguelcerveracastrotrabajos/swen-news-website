/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.ModelAuthentication;
import Models.ModelHashing;
import Models.ModelLog;
import Models.ModelMedia;
import Models.ModelReturnMessages;
import Models.ModelType;
import Models.ModelUser;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "Users", urlPatterns = {"/users"},
        initParams = {
                @WebInitParam(name = "id",value="*")
            }
        )
@MultipartConfig
public class Users extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser authU = new ModelUser();
            try {
                authU = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        try {
            // <editor-fold defaultstate="collapsed" desc="Initializing Class">
            ModelUser user = new ModelUser();
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Load all Users">
            if(request.getParameter("id")==null){
                //CREATE LOG REGISTING THE ACTION
                ModelLog log = new ModelLog("users", "View: Access Active Users", null, authU);
                log.create();
                //SEND VARIABLES
                request.setAttribute("contentPage", "users");
                //CHOOSE FILE TO SEND IT TO
                request.getRequestDispatcher("layout.jsp").forward(request,response);
            }
             // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Load a single User">
            else{
                user = user.findById(request.getParameter("id"));
                //CREATE LOG REGISTING THE ACTION
                ModelLog log = new ModelLog("users", "View: Access view User", request.getParameter("id"), authU);
                log.create();
                //SEND VARIABLES
                request.setAttribute("contentPage", "user");
                request.setAttribute("user", user);
                //CHOOSE FILE TO SEND IT TO
                request.getRequestDispatcher("layout.jsp").forward(request,response);
            }
            //</editor-fold>
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("users", "SQLException: Error Getting the Users "+ex, null, authU);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
           throw new RuntimeException("Error Getting the Users ", ex);
           //</editor-fold>
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser authU = new ModelUser();
            try {
                authU = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Setting the Correct Encoding and Respose Type">
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Initializing Class and Variables">
        ModelUser user = new ModelUser();
        ModelType type = new ModelType();
        ModelMedia media = new ModelMedia();
        ModelHashing hash = new ModelHashing();
        
        String message="";
        String userid=request.getParameter("id");
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Updating a single User">
        if(request.getParameter("username")!=null){
            try {
                user = user.findById(userid);
                media.updateById(user.getMedia().getMediaId(), user, request ,getServletContext().getRealPath("/"));
            } catch (SQLException ex) {
                Logger.getLogger(Users.class.getName()).log(Level.SEVERE, null, ex);
            }
            //ADD PARAMETERS TO OBJECT
            user.setUserName(request.getParameter("username"));
            user.setUserPassword(hash.hashPassword(request.getParameter("userpassword")));
            user.setUserEmail(request.getParameter("useremail"));
            
            type.setTypeId(request.getParameter("fktype"));
            user.setType(type);
            DateFormat  format = new SimpleDateFormat("yyyy-m-dd");
            try {
                java.util.Date utilStartDate = format.parse(request.getParameter("userbirthday"));
                java.sql.Date date = new java.sql.Date(utilStartDate.getTime());  
                user.setUserBirthday((java.sql.Date) date);
            } catch (ParseException ex) {
                // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
                user.setUserId("2a2f432c-5925-11e9-bcd0-086266b4ab53");
                ModelLog log = new ModelLog("users", "ParseException: Error Updating the user "+ex, userid, authU);
                try {
                    log.create();
                } catch (SQLException ex1) {
                    Logger.getLogger(Users.class.getName()).log(Level.SEVERE, null, ex1);
                }
                throw new RuntimeException("Error Creating the User "+ex);
                //</editor-fold>
            }
            try {
                user.updateById(authU);
            } catch (SQLException ex) {
                // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
                ModelLog log = new ModelLog("users", "SQLException: Error Updating the User "+ex, userid, authU);
                try {
                    log.create();
                } catch (SQLException ex1) {
                    Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
                }
               throw new RuntimeException("Error Updating the User ", ex);
               //</editor-fold>
            }
            message = "Successfully updating an user with the id of "+userid;
        }
        //</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Deleting a single User">
        else{
            try {
                user.deleteById(userid, authU);
            } catch (SQLException ex) {
                // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
                ModelLog log = new ModelLog("users", "SQLException: Error Deleting the User "+ex, userid, authU);
                try {
                    log.create();
                } catch (SQLException ex1) {
                    Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
                }
                throw new RuntimeException("Error Deleting the User ", ex);
                //</editor-fold>
            }
            message = "Successfully deleting an user with the id of "+userid;               
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Returning Message">
        Gson gson = new Gson();
        ModelReturnMessages returnElement = new ModelReturnMessages(message, new ArrayList<>());
        String jsonData = gson.toJson(returnElement);
        PrintWriter out = response.getWriter();
        try {
            out.println(jsonData);
        } finally {
            out.close();
        }
        //</editor-fold>
    }
}
