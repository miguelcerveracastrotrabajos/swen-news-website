/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.ModelArticle;
import Models.ModelAuthentication;
import Models.ModelLog;
import Models.ModelUser;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "OtherNews", urlPatterns = {"/main_other"})
public class OtherNews extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = new ModelUser();
            try {
                user = auth.setContentNormalUser(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(user.getUserId()==null){
                user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");
            }
        //</editor-fold>
       // <editor-fold default
        try {
            // <editor-fold defaultstate="collapsed" desc="Initializing Class">
            ModelArticle article = new ModelArticle();
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Load the view">
             //CREATE LOG REGISTING THE ACTION
            ModelLog log = new ModelLog("articles", "View: Access view Other", request.getParameter("id"), user);
            log.create();
            //SEND VARIABLES
            request.setAttribute("contentPage", "main_other");
            //CHOOSE FILE TO SEND IT TO
            request.getRequestDispatcher("layoutmain.jsp").forward(request,response);
            //</editor-fold>
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("articles", "SQLException: Error Getting the Articles "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Getting the Articles ", ex);
            //</editor-fold>
        }
    }
}
