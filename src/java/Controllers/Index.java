/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "Index", urlPatterns = {"/index"})
public class Index extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         // <editor-fold defaultstate="collapsed" desc="Load all Logs">
            //SEND VARIABLES
            request.setAttribute("contentPage", "index");
            //CHOOSE FILE TO SEND IT TO
            request.getRequestDispatcher("layout.jsp").forward(request,response);
        //</editor-fold>
    }

}
