/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;
import Models.ModelAuthentication;
import Models.ModelLog;
import Models.ModelUser;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author migue
 */
@WebServlet(name = "Logs", urlPatterns = {"/logs"})
public class Logs extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // <editor-fold defaultstate="collapsed" desc="Authentication">
            ModelAuthentication auth = new ModelAuthentication();
            ModelUser user = new ModelUser();
            try {
                user = auth.setContent(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        //</editor-fold>
        try {
            // <editor-fold defaultstate="collapsed" desc="Initializing Class">
            ModelLog log = new ModelLog();
            //</editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Load all Logs">
                //CREATE LOG REGISTING THE ACTION
                ModelLog logR = new ModelLog("types", "View: Access Logs", null, user);
                logR.create();
                //SEND VARIABLES
                request.setAttribute("contentPage", "logs");
                //CHOOSE FILE TO SEND IT TO
                request.getRequestDispatcher("layout.jsp").forward(request,response);
            //</editor-fold>
        } catch (SQLException ex) {
            // <editor-fold defaultstate="collapsed" desc="Saving and Showing the Error Given">
            ModelLog log = new ModelLog("logs", "ParseException: Error Viewing the Logs "+ex, null, user);
            try {
                log.create();
            } catch (SQLException ex1) {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new RuntimeException("Error Getting the Types "+ex);
            //</editor-fold>
        }
    }
}
