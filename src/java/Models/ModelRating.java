/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Timestamp;

/**
 *
 * @author migue
 */
public class ModelRating {
    //ATTRIBUTES
    private String _id;
    private Integer _rating;
    private Timestamp _created_at;
    private Timestamp _updated_at;
    //CONSTRUCTOR
    public ModelRating() {
    }
    public ModelRating(String _id, Integer _rating, Timestamp _created_at, Timestamp _updated_at) {
        this._id = _id;
        this._rating = _rating;
        this._created_at = _created_at;
        this._updated_at = _updated_at;
    }
    //GETTERS E SETTERS
    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public Integer getRating() {
        return _rating;
    }

    public void setRating(Integer _rating) {
        this._rating = _rating;
    }

    public Timestamp getCreated_at() {
        return _created_at;
    }

    public void setCreated_at(Timestamp _created_at) {
        this._created_at = _created_at;
    }

    public Timestamp getUpdated_at() {
        return _updated_at;
    }

    public void setUpdated_at(Timestamp _updated_at) {
        this._updated_at = _updated_at;
    }
    
    
    
}
