/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;
import java.sql.SQLException;
import java.sql.Timestamp;
import DataAccessObject.DataAccessType;
import java.util.List;
import java.util.UUID;
/**
 *
 * @author migue
 */
public class ModelType {
    //ATTRIBUTES
    private String _typeId;
    private String _typeDesignation;
    private String _typeDescription;
    private Integer _typeStatus;
    private Timestamp _created_at;
    private Timestamp _updated_at;
    //CONSTRUCTOR
    public ModelType() {
        
    }
    public ModelType(String _typeId, String _typeDesignation, String _typeDescription, Integer _typeStatus, Timestamp _created_at, Timestamp _updated_at) {
        this._typeId = _typeId;
        this._typeDesignation = _typeDesignation;
        this._typeDescription = _typeDescription;
        this._typeStatus = _typeStatus;
        this._created_at = _created_at;
        this._updated_at = _updated_at;
    }
    //GETTERS E SETTERS
    public String getTypeId() {
        return _typeId;
    }
    public void setTypeId(String _typeId) {
        this._typeId = _typeId;
    }
    public String getTypeDesignation() {
        return _typeDesignation;
    }
    public void setTypeDesignation(String _typeDesignation) {
        this._typeDesignation = _typeDesignation;
    }
    public String getTypeDescription() {
        return _typeDescription;
    }
    public void setTypeDescription(String _typeDescription) {
        this._typeDescription = _typeDescription;
    }
    public Integer getTypeStatus() {
        return _typeStatus;
    }
    public void setTypeStatus(Integer _typeStatus) {
        this._typeStatus = _typeStatus;
    }
    public Timestamp getCreated_at() {
        return _created_at;
    }
    public void setCreated_at(Timestamp _created_at) {
        this._created_at = _created_at;
    }
    public Timestamp getUpdated_at() {
        return _updated_at;
    }
    public void setUpdated_at(Timestamp _updated_at) {
        this._updated_at = _updated_at;
    }
    //Methods
    //Create Type
    public String create(ModelUser owner) throws SQLException{
       //ACCESS THE DOMAIN MODEL OF THE TYPE
       DataAccessType daoType = new DataAccessType();
       //CREATE A NEW UNIQUE IDENTIFIER
       String uniqueID = UUID.randomUUID().toString();
       //CREATE LOG REGISTING THE ACTION
       ModelLog log = new ModelLog("types", "Action: Create a Type", uniqueID, owner);
       log.create();
       //CREATE THE ACTUAL TYPE
       this.setTypeId(uniqueID);
       System.out.println(this.getTypeId()+" "+this.getTypeDesignation()+" "+this.getTypeDescription());
       daoType.create(this);
       //return the id
       return uniqueID;
    }
    //Update Type
    public void updateById(ModelUser owner) throws SQLException{
        //ACCESS THE DOMAIN MODEL OF THE TYPE
        DataAccessType daoType = new DataAccessType();
        //CREATE LOG REGISTING THE ACTION
        ModelLog log = new ModelLog("types", "Action: Update a Type", this.getTypeId(),owner);
        log.create();
        //UPDATE THE ACTUAL TYPE
        daoType.updateById(this);
    }
    //Delete Type
    public void deleteById(ModelUser owner)throws SQLException{
        //ACCESS THE DOMAIN MODEL OF THE ARTICLE
        DataAccessType daoType = new DataAccessType();
        //CREATE LOG REGISTING THE ACTION
        ModelLog log = new ModelLog("types", "Action: Delete a Type", this.getTypeId(),owner);
        log.create();
        //DELETE THE ACTUAL TYPE
        daoType.deleteById(this.getTypeId());
    }
    //Activate Type
    public void activateById(ModelUser owner)throws SQLException{
        //ACCESS THE DOMAIN MODEL OF THE ARTICLE
        DataAccessType daoType = new DataAccessType();
        //CREATE LOG REGISTING THE ACTION
        ModelLog log = new ModelLog("types", "Action: Activate a Type", this.getTypeId(), owner);
        log.create();
        //DELETE THE ACTUAL TYPE
        daoType.activateById(this.getTypeId());
    }
    //Find by Id
    public ModelType findById(String id) throws SQLException{
        DataAccessType daoType = new DataAccessType();
        return daoType.findById(id);
    }
    //List Active
    public List<ModelType> listActive() throws SQLException{
        DataAccessType daoType = new DataAccessType();
        return daoType.listActive();
    }
      //List Inactive
    public List<ModelType> listInactive() throws SQLException{
        DataAccessType daoType = new DataAccessType();
        return daoType.listInactive();
    }
}
