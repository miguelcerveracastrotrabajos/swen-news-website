/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.List;

/**
 *
 * @author migue
 */
public class ModelReturnMessages {
    //ATTRIBUTES
    private String _message;
    private List<String> _errorMessages;
    //CONSTRUTOR
    public ModelReturnMessages(String _message, List<String> _errorMessages) {
        this._message = _message;
        this._errorMessages = _errorMessages;
    }
    //GETTERS E SETTERS
    public String getMessage() {
        return _message;
    }

    public void setMessage(String _message) {
        this._message = _message;
    }

    public List<String> getErrorMessages() {
        return _errorMessages;
    }

    public void setErrorMessages(List<String> _errorMessages) {
        this._errorMessages = _errorMessages;
    }
    
}
