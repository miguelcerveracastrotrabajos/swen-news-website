/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;
import DataAccessObject.DataAccessMedia;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
/**
 *
 * @author migue
 */
public class ModelMedia {
    //ATTRIBUTES
    private String _mediaId;
    private String _mediaName;
    private String _mediaType;
    private Integer _mediaStatus;
    private Timestamp _created_at;
    private Timestamp _updated_at;
    //CONSTRUCTOR
    public ModelMedia() {
    }
    public ModelMedia(String _mediaId, String _mediaName, String _mediaType, Integer _mediaStatus, Timestamp _created_at, Timestamp _updated_at) {
        this._mediaId = _mediaId;
        this._mediaName = _mediaName;
        this._mediaType = _mediaType;
        this._mediaStatus = _mediaStatus;
        this._created_at = _created_at;
        this._updated_at = _updated_at;
    }
    //GETTERS E SETTERS
    public String getMediaId() {
        return _mediaId;
    }

    public void setMediaId(String _mediaId) {
        this._mediaId = _mediaId;
    }

    public String getMediaName() {
        return _mediaName;
    }

    public void setMediaName(String _mediaName) {
        this._mediaName = _mediaName;
    }

    public String getMediaType() {
        return _mediaType;
    }

    public void setMediaType(String _mediaType) {
        this._mediaType = _mediaType;
    }

    public Integer getMediaStatus() {
        return _mediaStatus;
    }

    public void setMediaStatus(Integer _mediaStatus) {
        this._mediaStatus = _mediaStatus;
    }

    public Timestamp getCreated_at() {
        return _created_at;
    }

    public void setCreated_at(Timestamp _created_at) {
        this._created_at = _created_at;
    }

    public Timestamp getUpdated_at() {
        return _updated_at;
    }

    public void setUpdated_at(Timestamp _updated_at) {
        this._updated_at = _updated_at;
    }
    //Methods
    //Create Media
    public String create(ModelUser user, HttpServletRequest request, String servletContext)throws SQLException, IOException, ServletException{
        //ACCESS THE DOMAIN MODEL OF THE ARTICLE
        DataAccessMedia daoMedia = new DataAccessMedia();
        //CREATE A NEW UNIQUE IDENTIFIER
        String uniqueID = UUID.randomUUID().toString();

        //CREATE LOG REGISTING THE ACTION
        ModelLog log = new ModelLog("medias", "Action: Create a Media", uniqueID, user);
        log.create();  
        //Upload File
        Part filePart =request.getPart("file");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        this.setMediaName(uniqueID);
        this.setMediaType(fileName.split(Pattern.quote("."))[1]);
        InputStream fileContent = filePart.getInputStream();
        File targetPath = new File(servletContext + "img");
        if (!targetPath.exists()) {
            targetPath.mkdirs();
        }   
        filePart.write(targetPath + File.separator + uniqueID + "." + fileName.split(Pattern.quote("."))[1]);
        //CREATE THE ACTUAL ARTICLE
        this.setMediaId(uniqueID);
        daoMedia.create(this);
        //return the id
        return uniqueID;
    }
    //Update Media
    public String updateById(String uniqueID, ModelUser user, HttpServletRequest request, String servletContext) throws SQLException, IOException, ServletException{
        //ACCESS THE DOMAIN MODEL OF THE ARTICLE
        DataAccessMedia daoMedia = new DataAccessMedia();
        //CREATE LOG REGISTING THE ACTION
        ModelLog log = new ModelLog("medias", "Action: Update a Media", uniqueID, user);
        log.create();
        //UPLOAD FILE
        Part filePart =request.getPart("file");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        InputStream fileContent = filePart.getInputStream();
        File targetPath = new File(servletContext + "img");
        if (!targetPath.exists()) {
            targetPath.mkdirs();
        }   
        filePart.write(targetPath + File.separator + uniqueID + "." + fileName.split(Pattern.quote("."))[1]);
        //UPDATE THE ACTUAL ARTICLE
        daoMedia.updateById(this);
        //return the id
        return uniqueID;
    }
    //Delete Media
    public void deleteById(ModelUser user)throws SQLException{
        //ACCESS THE DOMAIN MODEL OF THE ARTICLE
        DataAccessMedia daoMedia = new DataAccessMedia();
        //CREATE LOG REGISTING THE ACTION
        ModelLog log = new ModelLog("medias", "Action: Delete a Media", this.getMediaId(), user);
        log.create();
        //DELETE THE ACTUAL ARTICLE
        daoMedia.deleteById(this.getMediaId());
    }
    //Find by Id
    public ModelMedia findById(String id) throws SQLException{
        DataAccessMedia daoMedia = new DataAccessMedia();
        return daoMedia.findById(id);
    }
    //List Active
    public List<ModelMedia> listActive() throws SQLException{
        DataAccessMedia daoMedia = new DataAccessMedia();
        return daoMedia.listActive();
    }

    private Object getServletContext() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
