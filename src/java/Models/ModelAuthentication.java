/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author migue
 */
public class ModelAuthentication {
    //CONSTRUTOR
    public ModelAuthentication() {
    }
    //Methods
    //BackOffice Authentication
    public void authenticationUser(HttpServletRequest request,  HttpServletResponse response) throws SQLException, IOException, ServletException{
        // <editor-fold defaultstate="collapsed" desc="Initializing Classes">
        ModelUser user = new ModelUser();
        ModelHashing hash = new ModelHashing();
        HttpSession httpSession = request.getSession();
        //</editor-fold>
        if(request.getParameter("useremail")!=null){
            user=user.authenticateByEmail(request.getParameter("useremail"));

            if(user.getUserId()!=null){                
                if(hash.checkPassword(request.getParameter("userpassword"), user.getUserPassword()) && new String(user.getType().getTypeId()).equals("ec29656b-1721-44c7-9b87-e9c8278bb45e")){
                    ModelLog log = new ModelLog("users", "Action: Authentication Success", null, user);
                    log.create();
                    httpSession.setAttribute("userid", user.getUserId());
                    response.sendRedirect("/ProjetoArtigos/articles");
                }
                else{
                    user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");                                                             //UNAUTHENTICATED USER
                    ModelLog log = new ModelLog("users", "Action: Authentication Failed by Password", null, user);
                    log.create();
                    response.sendRedirect("/ProjetoArtigos/users_authenticate");
                }
            }
            else{
                user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");                                                                 //UNAUTHENTICATED USER
                ModelLog log = new ModelLog("users", "Action: Authentication Failed by Email", null, user);
                log.create();
                response.sendRedirect("/ProjetoArtigos/users_authenticate");
            }
        }
        
    }
    public void unauthenticateUser(HttpServletRequest request,  HttpServletResponse response) throws IOException{
        HttpSession httpSession = request.getSession();
        httpSession.setAttribute("userid", null);
        httpSession.invalidate();
        response.sendRedirect("/ProjetoArtigos/index");
    }
    public ModelUser setContent(HttpServletRequest request,  HttpServletResponse response) throws SQLException, IOException, ServletException{
        HttpSession httpSession = request.getSession();
        String userid=(String)httpSession.getAttribute("userid");
        ModelUser user = new ModelUser();
        if(userid!=null){
            user = user.findById(userid);  
            request.setAttribute("auth", user);
        }
        if(user.getUserId()==null){
            //SEND VARIABLES
            request.setAttribute("contentPage", "index");
            //CHOOSE FILE TO SEND IT TO
            request.getRequestDispatcher("layout.jsp").forward(request,response);
        }
        return user;
    }
    
    //NORMAL AUTHENTICATION
    public ModelUser setContentNormalUser(HttpServletRequest request,  HttpServletResponse response) throws SQLException, IOException, ServletException{
        HttpSession httpSession = request.getSession();
        String userid=(String)httpSession.getAttribute("userid");
        ModelUser user = new ModelUser();
        if(userid!=null){
            user = user.findById(userid);  
            request.setAttribute("auth", user);
        }
        return user;
    }
    public void authenticationNormalUser(HttpServletRequest request,  HttpServletResponse response) throws SQLException, IOException, ServletException{
        // <editor-fold defaultstate="collapsed" desc="Initializing Classes">
        ModelUser user = new ModelUser();
        ModelHashing hash = new ModelHashing();
        HttpSession httpSession = request.getSession();
        //</editor-fold>
        if(request.getParameter("useremail")!=null){
            user=user.authenticateByEmail(request.getParameter("useremail"));
            if(user.getUserId()!=null){
                
                if(hash.checkPassword(request.getParameter("userpassword"), user.getUserPassword()) && new String(user.getType().getTypeId()).equals("d0ab048b-3e93-4695-a84d-c3c45d0775bb")){
                    ModelLog log = new ModelLog("users", "Action: Authentication Success", null, user);
                    log.create();
                    httpSession.setAttribute("userid", user.getUserId());
                    response.sendRedirect("/ProjetoArtigos/");
                }
                else{
                    user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");                                                             //UNAUTHENTICATED USER
                    ModelLog log = new ModelLog("users", "Action: Authentication Failed by Password", null, user);
                    log.create();
                    response.sendRedirect("/ProjetoArtigos/main_login");
                }
            }
            else{
                user.setUserId("675685ef-633d-49fa-87c3-ba02c9f5e618");                                                                 //UNAUTHENTICATED USER
                ModelLog log = new ModelLog("users", "Action: Authentication Failed by Email", null, user);
                log.create();
                response.sendRedirect("/ProjetoArtigos/main_login");
            }
        }
        
    }
    public void unauthenticateNormalUser(HttpServletRequest request,  HttpServletResponse response) throws IOException{
        HttpSession httpSession = request.getSession();
        httpSession.setAttribute("userid", null);
        httpSession.invalidate();
        response.sendRedirect("/ProjetoArtigos");
    }
    
}
