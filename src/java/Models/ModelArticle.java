/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;
import DataAccessObject.DataAccessArticle;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
/**
 *
 * @author migue
 */
public class ModelArticle implements Comparable<ModelArticle>{
    //ATTRIBUTES
    private String _articleId;
    private String _articleTitle;
    private String _articleSubtitle;
    private String _articleBody;
    private Integer _articleStatus;
    private Integer _articleViews;
    private ModelUser _author;
    private ModelMedia _image;
    private Timestamp _created_at;
    private Timestamp _updated_at;
    //CONSTRUCTOR
    public ModelArticle() {
    }
    public ModelArticle(String _articleId, String _articleTitle, String _articleSubtitle, String _articleBody, Integer _articleStatus, ModelUser _author, ModelMedia _image, Timestamp _created_at, Timestamp _updated_at) {
        this._articleId = _articleId;
        this._articleTitle = _articleTitle;
        this._articleSubtitle = _articleSubtitle;
        this._articleBody = _articleBody;
        this._articleStatus = _articleStatus;
        this._author = _author;
        this._image = _image;
        this._created_at = _created_at;
        this._updated_at = _updated_at;
    }
    //GETTERS E SETTERS
    public String getArticleId() {
        return _articleId;
    }

    public void setArticleId(String _articleId) {
        this._articleId = _articleId;
    }

    public String getArticleTitle() {
        return _articleTitle;
    }

    public void setArticleTitle(String _articleTitle) {
        this._articleTitle = _articleTitle;
    }

    public String getArticleSubtitle() {
        return _articleSubtitle;
    }

    public void setArticleSubtitle(String _articleSubtitle) {
        this._articleSubtitle = _articleSubtitle;
    }

    public String getArticleBody() {
        return _articleBody;
    }

    public void setArticleBody(String _articleBody) {
        this._articleBody = _articleBody;
    }

    public Integer getArticleStatus() {
        return _articleStatus;
    }

    public void setArticleStatus(Integer _articleStatus) {
        this._articleStatus = _articleStatus;
    }

    public ModelUser getAuthor() {
        return _author;
    }

    public void setAuthor(ModelUser _author) {
        this._author = _author;
    }

    public ModelMedia getImage() {
        return _image;
    }

    public void setImage(ModelMedia _image) {
        this._image = _image;
    }

    public Timestamp getCreated_at() {
        return _created_at;
    }

    public void setCreated_at(Timestamp _created_at) {
        this._created_at = _created_at;
    }

    public Timestamp getUpdated_at() {
        return _updated_at;
    }

    public void setUpdated_at(Timestamp _updated_at) {
        this._updated_at = _updated_at;
    }

    public Integer getArticleViews() {
        return _articleViews;
    }

    public void setArticleViews(Integer _articleViews) {
        this._articleViews = _articleViews;
    }
    
    //Methods
    //Create Article
    public String create(ModelUser owner)throws SQLException{
       //ACCESS THE DOMAIN MODEL OF THE ARTICLE
       DataAccessArticle daoArticle = new DataAccessArticle();
       //CREATE A NEW UNIQUE IDENTIFIER
       String uniqueID = UUID.randomUUID().toString();
       //CREATE LOG REGISTING THE ACTION
       ModelLog log = new ModelLog("articles", "Action: Create an Article", uniqueID, owner);
       log.create();
       //CREATE THE ACTUAL ARTICLE
       this.setArticleId(uniqueID);
       daoArticle.create(this);
       //return the id
       return uniqueID;
    }
    //Update Article
    public void updateById(ModelUser owner) throws SQLException{
        
        //ACCESS THE DOMAIN MODEL OF THE ARTICLE
        DataAccessArticle daoArticle = new DataAccessArticle();
        //CREATE LOG REGISTING THE ACTION
        ModelLog log = new ModelLog("articles", "Action: Update an Article", this.getArticleId(),owner);
        log.create();
        //UPDATE THE ACTUAL ARTICLE
        daoArticle.updateById(this);
    }
    //Delete Artocçe
    public void deleteById(ModelUser owner)throws SQLException{
        //ACCESS THE DOMAIN MODEL OF THE ARTICLE
        DataAccessArticle daoArticle = new DataAccessArticle();
        //CREATE LOG REGISTING THE ACTION
        ModelLog log = new ModelLog("articles", "Action: Delete an Article", this.getArticleId(), owner);
        log.create();
        //DELETE THE ACTUAL ARTICLE
        daoArticle.deleteById(this.getArticleId());
    }
    //Activate Article
    public void activateById(ModelUser owner)throws SQLException{
        //ACCESS THE DOMAIN MODEL OF THE ARTICLE
        DataAccessArticle daoArticle = new DataAccessArticle();
        //CREATE LOG REGISTING THE ACTION
        ModelLog log = new ModelLog("articles", "Action: Activate an Article", this.getArticleId(), owner);
        log.create();
        //DELETE THE ACTUAL ARTICLE
        daoArticle.activateById(this.getArticleId());
    }
    //Find by Id
    public ModelArticle findById(String id) throws SQLException{
        DataAccessArticle daoArticle = new DataAccessArticle();
        ModelArticle article = daoArticle.findById(id);
        //Set Author Object
        String userid= article.getAuthor().getUserId();
        ModelUser user=new ModelUser();
        article.setAuthor(user.findById(userid));
        //Set Media Object
        String mediaid= article.getImage().getMediaId();
        ModelMedia media=new ModelMedia();
        article.setImage(media.findById(mediaid));
        return article;
    }
    //List Active
    public List<ModelArticle> listActive() throws SQLException{
        DataAccessArticle daoArticle = new DataAccessArticle();
        List<ModelArticle> articles = daoArticle.listActive();
        for (ModelArticle article : articles) {
            //Set Author Object
            String userid= article.getAuthor().getUserId();
            ModelUser user=new ModelUser();
            article.setAuthor(user.findById(userid));
            //Set Media Object
            String mediaid= article.getImage().getMediaId();
            ModelMedia media=new ModelMedia();
            article.setImage(media.findById(mediaid));
        }
        return articles;
    }
     //List Inactive
    public List<ModelArticle> listInactive() throws SQLException{
        DataAccessArticle daoArticle = new DataAccessArticle();
            List<ModelArticle> articles = daoArticle.listInactive();
        for (ModelArticle article : articles) {
            //Set Author Object
            String userid= article.getAuthor().getUserId();
            ModelUser user=new ModelUser();
            article.setAuthor(user.findById(userid));
            //Set Media Object
            String mediaid= article.getImage().getMediaId();
            ModelMedia media=new ModelMedia();
            article.setImage(media.findById(mediaid));
        }
        return articles;
    }
    public int compareTo(ModelArticle o) {
        return (o.getArticleViews() - this.getArticleViews());
    }
}
