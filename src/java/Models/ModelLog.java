/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import DataAccessObject.DataAccessLog;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author migue
 */
public class ModelLog {
    //ATTRIBUTES
    private String _logId;
    private String _logDesignation;
    private String _logDescription;
    private String _logElement;
    private ModelUser _logUser;
    private Timestamp _created_at;
    private Timestamp _updated_at;
    //CONSTRUTOR
    public ModelLog() {
    }
    public ModelLog(String _logDesignation, String _logDescription, String _logElement, ModelUser _logUser) {
        this._logDesignation = _logDesignation;
        this._logDescription = _logDescription;
        this._logElement = _logElement;
        this._logUser = _logUser;
    }
    //GETTERS E SETTERS
    public String getLogId() {
        return _logId;
    }

    public void setLogId(String _logId) {
        this._logId = _logId;
    }

    public String getLogDesignation() {
        return _logDesignation;
    }

    public void setLogDesignation(String _logDesignation) {
        this._logDesignation = _logDesignation;
    }

    public String getLogDescription() {
        return _logDescription;
    }

    public void setLogDescription(String _logDescription) {
        this._logDescription = _logDescription;
    }

    public String getLogElement() {
        return _logElement;
    }

    public void setLogElement(String _logElement) {
        this._logElement = _logElement;
    }

    public ModelUser getLogUser() {
        return _logUser;
    }

    public void setLogUser(ModelUser _logUser) {
        this._logUser = _logUser;
    }
    
    public Timestamp getCreated_at() {
        return _created_at;
    }

    public void setCreated_at(Timestamp _created_at) {
        this._created_at = _created_at;
    }

    public Timestamp getUpdated_at() {
        return _updated_at;
    }

    public void setUpdated_at(Timestamp _updated_at) {
        this._updated_at = _updated_at;
    }
    //Methods
    //Create Log
    public void create()throws SQLException{
       DataAccessLog daoLog = new DataAccessLog();
       daoLog.create(this);
    }
    //List View
    public List<ModelLog> listViewLogs() throws SQLException{
        DataAccessLog daoLog = new DataAccessLog();
        return daoLog.listViewLogs();
    }
    //List Action
    public List<ModelLog> listActionLogs() throws SQLException{
        DataAccessLog daoLog = new DataAccessLog();
        return daoLog.listActionLogs();
    }
    //List Error
    public List<ModelLog> listErrorLogs() throws SQLException{
        DataAccessLog daoLog = new DataAccessLog();
        return daoLog.listErrorLogs();
    }
    //List Action Logs
    public List<Integer> getGraphsAndStats() throws SQLException{
        DataAccessLog daoLog = new DataAccessLog();
        return daoLog.getGraphsAndStats();
    }
    //Articles per user
    public List<String> getArticlesPerUser() throws SQLException{
        DataAccessLog daoLog = new DataAccessLog();
        return daoLog.getArticlesPerUser();
    }
    //Users per type
    public List<String> getUsersPerType() throws SQLException{
        DataAccessLog daoLog = new DataAccessLog();
        return daoLog.getUsersPerType();
    }
    //Views per Article
    public List<String> getViewsPerArticle() throws SQLException{
        DataAccessLog daoLog = new DataAccessLog();
        return daoLog.getViewsPerArticle();
    }
    //Views per User
    public List<String> getViewsPerUser() throws SQLException{
        DataAccessLog daoLog = new DataAccessLog();
        return daoLog.getViewsPerUser();
    }
    //Logs per Day
    public List<String> getLogsPerDay() throws SQLException{
        DataAccessLog daoLog = new DataAccessLog();
        return daoLog.getLogsPerDay();
    }
    //Articles per Day
    public List<String> getArticlesPerDay() throws SQLException{
        DataAccessLog daoLog = new DataAccessLog();
        return daoLog.getArticlesPerDay();
    }
}
