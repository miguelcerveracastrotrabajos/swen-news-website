/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;
import DataAccessObject.DataAccessUser;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
/**
 *
 * @author migue
 */
public class ModelUser {
    //ATTRIBUTES
    private String _userId;
    private String _userName;
    private Date _userBirthday;
    private String _userEmail;
    private String _userPassword;
    private Integer _userStatus;
    private ModelType _type;
    private ModelMedia _media;
    private Timestamp _created_at;
    private Timestamp _updated_at;
    //CONSTRUTOR
    public ModelUser() {
    }
    public ModelUser(String _userId, String _userName, Date _userBirthday, String _userEmail, String _userPassword, Integer _userStatus, ModelType _type, ModelMedia _image, Timestamp _created_at, Timestamp _updated_at) {
        this._userId = _userId;
        this._userName = _userName;
        this._userBirthday = _userBirthday;
        this._userEmail = _userEmail;
        this._userPassword = _userPassword;
        this._userStatus = _userStatus;
        this._type = _type;
        this._created_at = _created_at;
        this._updated_at = _updated_at;
    }
    
    //GETTERS E SETTERS
    public String getUserId() {
        return _userId;
    }

    public void setUserId(String _userId) {
        this._userId = _userId;
    }

    public String getUserName() {
        return _userName;
    }

    public void setUserName(String _userName) {
        this._userName = _userName;
    }

    public Date getUserBirthday() {
        return _userBirthday;
    }

    public void setUserBirthday(Date _userBirthday) {
        this._userBirthday = _userBirthday;
    }

    public String getUserEmail() {
        return _userEmail;
    }

    public void setUserEmail(String _userEmail) {
        this._userEmail = _userEmail;
    }

    public String getUserPassword() {
        return _userPassword;
    }

    public void setUserPassword(String _userPassword) {
        this._userPassword = _userPassword;
    }

    public Integer getUserStatus() {
        return _userStatus;
    }

    public void setUserStatus(Integer _userStatus) {
        this._userStatus = _userStatus;
    }

    public ModelType getType() {
        return _type;
    }

    public void setType(ModelType _type) {
        this._type = _type;
    }

    public Timestamp getCreated_at() {
        return _created_at;
    }

    public void setCreated_at(Timestamp _created_at) {
        this._created_at = _created_at;
    }

    public Timestamp getUpdated_at() {
        return _updated_at;
    }

    public void setUpdated_at(Timestamp _updated_at) {
        this._updated_at = _updated_at;
    }

    public ModelMedia getMedia() {
        return _media;
    }

    public void setMedia(ModelMedia _media) {
        this._media = _media;
    }
    
    //Methods
    //Create User
    public String create(ModelUser owner)throws SQLException{
        //ACCESS THE DOMAIN MODEL OF THE TYPE
       DataAccessUser daoUser = new DataAccessUser();
       //CREATE A NEW UNIQUE IDENTIFIER
       String uniqueID = UUID.randomUUID().toString();
       //SET THE ID
       this.setUserId(uniqueID);
       //CREATE LOG REGISTING THE ACTION
       ModelLog log = new ModelLog("users", "Action: Create a User", uniqueID, owner);
       //CREATE THE ACTUAL USER
       daoUser.create(this);
       //CREATE THE LOG
       log.create();
       //return the id
       return uniqueID;
    }
    //Update User
    public void updateById(ModelUser owner) throws SQLException{
        //ACCESS THE DOMAIN MODEL OF THE TYPE
        DataAccessUser daoUser = new DataAccessUser();
        //CREATE LOG REGISTING THE ACTION
        ModelLog log = new ModelLog("users", "Action: Update a user", this.getUserId(), owner);
        log.create();
        //UPDATE THE ACTUAL USER
        daoUser.updateById(this);
    }
    //Delete User
    public void deleteById(String id, ModelUser owner)throws SQLException{
        //ACCESS THE DOMAIN MODEL OF THE ARTICLE
        DataAccessUser daoUser = new DataAccessUser();
        //CREATE LOG REGISTING THE ACTION
        ModelLog log = new ModelLog("users", "Action: Delete a user", id, owner);
        log.create();
        this.setUserId(id);
        //DELETE THE ACTUAL USER
        daoUser.deleteById(id);
    }
    //Activate User
    public void activateById(String id, ModelUser owner)throws SQLException{
        //ACCESS THE DOMAIN MODEL OF THE ARTICLE
        DataAccessUser daoUser = new DataAccessUser();
        //CREATE LOG REGISTING THE ACTION
        ModelLog log = new ModelLog("users", "Action: Activate a user", id, owner);
        log.create();
        this.setUserId(id);
        //DELETE THE ACTUAL USER
        daoUser.activateById(id);
    }
    //Find by Id ?
    public ModelUser findById(String id) throws SQLException{
        DataAccessUser daoUser = new DataAccessUser();
        return daoUser.findById(id);
    }
    //Authenticate by email
    public ModelUser authenticateByEmail(String email) throws SQLException{
        DataAccessUser daoUser = new DataAccessUser();
        return daoUser.authenticateByEmail(email);
    }
    //List Active
    public List<ModelUser> listActive() throws SQLException{
        DataAccessUser daoUser = new DataAccessUser();
        List<ModelUser> users = daoUser.listActive();
        for (ModelUser user : users) {
            //Set Type Object
            String typeid= user.getType().getTypeId();
            ModelType type=new ModelType();
            user.setType(type.findById(typeid));
        }
        return users;
    }
    //List Inactive
    public List<ModelUser> listInactive() throws SQLException{
        DataAccessUser daoUser = new DataAccessUser();
        List<ModelUser> users = daoUser.listInactive();
        for (ModelUser user : users) {
            //Set Type Object
            String typeid= user.getType().getTypeId();
            ModelType type=new ModelType();
            user.setType(type.findById(typeid));
        }
        return users;
    }
}
