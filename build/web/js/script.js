$(window).on('load', function(){
    //CODIGO JS
    $('#btnHi').click(function(){
        //PARA TESTAR
        //updateArticle("0d975dbc-f2f0-44cf-9067-4254e6fb4c2a", "Fun", "Diversão Teste por Ajax", "Teste por Ajax");
        //insertArticle("AQUI", "VAI", "DAR");
        //deleteArticle("0d975dbc-f2f0-44cf-9067-4254e6fb4c2a");
        //insertTypeUser("teste", "teste");
        //updateTypeUser("de780863-f758-47ce-81c7-f9970aeb5ef7", "XXX", "xxx")
        //deleteTypeUser("de780863-f758-47ce-81c7-f9970aeb5ef7");
        //insertUser("Gerbil","1997-09-11xx", "mail@mail.com", "123", "48622e2d-5919-11e9-bcd0-086266b4ab53");
        //updateUser("6ab022ab-4a5f-46af-8008-32cf64c2a756","Gerbil2","1997-09-11", "mail@mail.com", "123", "48622e2d-5919-11e9-bcd0-086266b4ab53")
        //deleteUser("6ab022ab-4a5f-46af-8008-32cf64c2a756");
    })
});
//ELEMENT FUNCTIONS
$('#inputGroupFile01').on('change',function(){
    //get the file name
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $(this).next('.custom-file-label').html(fileName);
})
//ARTICLE FUNCTIONS
    //INSERT
    function insertArticle(data){
        $.ajax({
            type:'POST',
            data:data,
            processData: false,
            contentType: false,
            cache: false,
            url:'/ProjetoArtigos/articles_create',
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                }
                window.location.href = "/ProjetoArtigos/articles";
            }
         });
    }
    //UPDATE
    function updateArticle(data){
        $.ajax({
            type:'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            url:'/ProjetoArtigos/articles?id='+data.get("articleid"),
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                }
                window.location.href = "/ProjetoArtigos/articles";
            }
         });
    }
    //DELETE
    function deleteArticle(data){
        $.ajax({
            type:'POST',
            url:'/ProjetoArtigos/articles?id='+data.get("articleid"),
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                }
                window.location.href = "/ProjetoArtigos/articles";
            }
         });
    }
    //ACTIVATE
    function activateArticle(data){
        $.ajax({
            type:'POST',
            url:'/ProjetoArtigos/articles_activate?id='+data.get("articleid"),
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                }
                window.location.href = "/ProjetoArtigos/articles";
            }
         });
    }
//TYPE FUNCTIONS
    //INSERT
    function insertTypeUser(data){
        $.ajax({
            type:'POST',
            data:data,
            processData: false,
            contentType: false,
            cache: false,
            url:'/ProjetoArtigos/types_create',
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                }
                window.location.href = "/ProjetoArtigos/types";
            }
        });
    }
    //UPDATE
    function updateTypeUser(data){
        $.ajax({
            type:'POST',
            data:data,
            processData: false,
            contentType: false,
            cache: false,
            url:'/ProjetoArtigos/types?id='+data.get("typeid"),
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                }
            }
        });
    }
    //DELETE
    function deleteTypeUser(data){
        $.ajax({
            type:'POST',
            processData: false,
            contentType: false,
            cache: false,
            url:'/ProjetoArtigos/types?id='+data,
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                }
                window.location.href = "/ProjetoArtigos/types";
            }
         });
    }
    //ACTIVATE
    function activateTypeUser(data){
        $.ajax({
            type:'POST',
            url:'/ProjetoArtigos/types_activate?id='+data.get("typeid"),
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                }
                window.location.href = "/ProjetoArtigos/types";
            }
         });
    }
//USER FUNCTIONS
    //INSERT
    function insertUser(data){
        $.ajax({
            type:'POST',
            data:data,
            processData: false,
            contentType: false,
            cache: false,
            url:'/ProjetoArtigos/users_create',
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                }
                window.location.href = "/ProjetoArtigos/users";
            }
        });
    }
    //UPDATE
    function updateUser(data){
        $.ajax({
            type:'POST',
            data:data,
            processData: false,
            contentType: false,
            cache: false,
            url:'/ProjetoArtigos/users?id='+data.get("userid"),
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                }
                window.location.href = "/ProjetoArtigos/users";
            }
        });
    }
    //DELETE
    function deleteUser(data){
        $.ajax({
            type:'POST',
            url:'/ProjetoArtigos/users?id='+data.get("userid"),
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                }
                window.location.href = "/ProjetoArtigos/users";
            }
         });
    }
    //ACTIVATE
    function activateUser(data){
        $.ajax({
            type:'POST',
            url:'/ProjetoArtigos/users_activate?id='+data.get("userid"),
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                }
                 window.location.href = "/ProjetoArtigos/users";
            }
         });
    }
    //AUTHENTICATE
    function authenticateUserX(data){
        console.log(data);
        $.ajax({
            type:'POST',
            data:data,
            processData: false,
            contentType: false,
            cache: false,
            url:'/ProjetoArtigos/users_authenticate',
            success: function(result){
                if(result._errorMessages.length==0){
                    console.log(result._message);
                    if(result._message=="Success Login"){
                        //window.location.href = "/ProjetoArtigos/articles";
                    }
                }
            }
        });
    }
    
