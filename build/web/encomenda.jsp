<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>ENCOMENDA</h1>
        <form class="form-group" action="encomenda.jsp">
        <table class="table">
        <tr>
          <th>Author</th>
          <th>TItle</th>
          <th>Price</th>
          <th>Stock</th>
        </tr>
        <%
            String[] ids = request.getParameterValues("selected");
            for (String id : ids) 
            { 
                //Configuration Database Variables
                Class.forName("com.mysql.jdbc.Driver");
                Connection connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/university","root",""); 
                PreparedStatement preparedstmt = null;
                ResultSet rs = null;
                preparedstmt = connect.prepareStatement("call university.SP_Books_UpdateStock(?)");
                preparedstmt.setString(1, id);
                rs = preparedstmt.executeQuery();
                while (rs.next()) {
                    %>
                        <tr>
                            <td><%= rs.getString("booksAuthor")%></td>
                            <td><%= rs.getString("booksTitle")%></td>
                            <td><%= rs.getFloat("booksPrice")%></td>
                            <td><%= rs.getInt("booksStock")%></td>
                        </tr>
                    <%
                    
                }
                rs.close();
                preparedstmt.close();
                connect.close();
            }
        %>
        <input type="submit" value="Query" class="btn btn-danger">
        </form>
    </body>
</html>