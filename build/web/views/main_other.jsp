 
<%@page import="java.util.Collections"%>
<%@page import="java.util.List"%>
<%@page import="Models.ModelArticle"%>
<%@page import="Models.ModelMedia"%>
<!-- ##### Hero Area Start ##### -->
    <% 
        ModelArticle u = new ModelArticle();

        List<ModelArticle> lstactive = u.listActive(); 
        
    %>
    
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Mag Posts Area Start ##### -->
    <section class="mag-posts-area d-flex flex-wrap">

        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Left Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="post-sidebar-area left-sidebar mt-30 mb-30 bg-white box-shadow">
            <!-- Sidebar Widget -->
            <div class="single-sidebar-widget p-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>Most Popular</h5>
                </div>
                <div >
                    <%  
                       
                        if(lstactive!=null) {
                            List<ModelArticle> newlstactive =lstactive;
                            Collections.sort(newlstactive);
                            for (ModelArticle article : newlstactive) {     

                    %>
                    <!-- Single Blog Post -->
                    <div class="single-blog-post d-flex">
                        <div class="post-thumbnail">
                            <% if(new String(article.getImage().getMediaType()).equals("mp4")){ %> 
                                <video style="width:100%;">
                                    <source src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>" type="video/<%= article.getImage().getMediaType() %>">
                                 </video>
                            <% } else {%>
                                <img src="img/<%=article.getImage().getMediaId()+"."+article.getImage().getMediaType() %>">
                            <% } %>
                        </div>
                        <div class="post-content">
                            <a href="/ProjetoArtigos/main_article?id=<%= article.getArticleId()%>" class="post-title"> <%= article.getArticleTitle() %> </a>
                            <span class="mini-tag">
                                <i class="fa fa-eye" aria-hidden="true"></i><%= article.getArticleViews() %>
                            </span>
                        </div>
                    </div>
                    <%
                            }
                        }
                    %>
                </div>
            </div>
        </div>

        <!-- >>>>>>>>>>>>>>>>>>>>
             Main Posts Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="mag-posts-content mt-30 mb-30 p-30 box-shadow">
                <h1>Other Headlines</h1>
                <h6>Powered by a Node.JS Express API using the <a href="https://cheerio.js.org/" class="post-title">Cheerio Module</a></h6>
                <img src="https://cdn.worldvectorlogo.com/logos/nodejs-icon.svg" style="margin-top: -100px;height: 75px;float: right;">
                <!-- BBC NEWS -->
                <div class="section-heading">
                    <h5><a href="https://www.bbc.com/news"><img src="https://img.icons8.com/metro/420/bbc-logo.png" style="height:50px;"></a> <span class="number_title"><span id="number_bbc">0</span> More Articles</span></h5>
                </div>
                <div class="bbc">
                    <div class="loader"></div>
                </div>
                <!-- BBC NEWS -->
                <div class="section-heading">
                    <h5><a href="https://pplware.sapo.pt/"><img src="https://pplware.sapo.pt/wp-content/themes/namek/img/logo-pplware.svg" style="height:50px; max-width: 88px;background-color: black;border-radius: 4px;"></a> <span class="number_title"><span id="number_pplware">0</span> More Articles</span></h5>
                </div>
                <div class="pplware">
                    <div class="loader"></div>
                </div>
                 <!-- BBC NEWS -->
                <div class="section-heading">
                    <h5><a href="https://www.nytimes.com/"><img src="http://www.barbaraprey.com/wp/wp-content/uploads/2011/12/The-New-York-Times-icon.png" style="height:50px; max-width: 88px;border:1px solid grey;border-radius: 4px;"></a><span class="number_title"><span id="number_other">0</span> More Articles</span></h5>
                </div>
                <div class="other">
                    <div class="loader"></div>
                </div>
        </div>
        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Right Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="post-sidebar-area right-sidebar mt-30 mb-30 box-shadow bg-white">
                <!-- Sidebar Widget -->
                <div class="single-sidebar-widget p-30">
                    <!-- Social Followers Info -->
                    <div class="social-followers-info">
                        <!-- Facebook -->
                        <a href="#" class="facebook-fans"><i class="fa fa-facebook"></i> 4,360 <span>Fans</span></a>
                        <!-- Twitter -->
                        <a href="#" class="twitter-followers"><i class="fa fa-twitter"></i> 3,280 <span>Followers</span></a>
                        <!-- YouTube -->
                        <a href="#" class="youtube-subscribers"><i class="fa fa-youtube"></i> 1250 <span>Subscribers</span></a>
                        <!-- Google -->
                        <a href="#" class="google-followers"><i class="fa fa-google-plus"></i> 4,230 <span>Followers</span></a>
                    </div>
                </div>
    
                
    
                <!-- Sidebar Widget -->
                <div class="single-sidebar-widget p-30">
                    <!-- Section Title -->
                    <div class="section-heading">
                        <h5>About Us</h5>
                    </div>
    
                    <div class="newsletter-form">
                        <p>Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                Subscribe our newsletter gor get notification about new updates, information discount, etc.
                                
                        </p>
                    </div>
                     <div class="button-back-article"><a href="/ProjetoArtigos/main_aboutus" class="btn mag-btn w-100" style="    margin-left: 25%;">See More</a></div>
                </div>
            </div>
        
    </section>
    <!-- ##### Mag Posts Area End ##### -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script>
        
       getOtherPlatformNews();
    </script>