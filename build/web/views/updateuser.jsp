<%@page import="Models.ModelType"%>
<%@page import="Models.ModelUser"%>
<%@page import="java.util.List"%>
<% 
    ModelUser user = new ModelUser();
    user = (ModelUser)request.getAttribute("user");
%>
<form class="updateUser" enctype="multipart/form-data" onsubmit="event.preventDefault(); updateUser(new FormData($(this)[0]))">
    <h1>Update an User</h1>
    <div style="text-align:center;">
        <img src="img/<%=user.getMedia().getMediaId()+"."+user.getMedia().getMediaType() %>" style="max-width:350px;max-height:350px;">
        <input name="userid" type="hidden" value="<%= user.getUserId() %>">
    </div>
    <div class="form-group">
        <label for="username">Name</label>
        <input class="form-control username" id="username" name="username" placeholder="Enter the name" value="<%= user.getUserName()%>">
    </div>
    <div class="form-group">
        <label for="userbirthday">Birthday</label>
        <input class="form-control userbirthday" id="userbirthday" name="userbirthday" placeholder="Enter the birthday" value="<%= user.getUserBirthday()%>">
    </div>
    <div class="form-group">
        <label for="useremail">Email</label>
        <input class="form-control useremail" id="useremail" name="useremail" placeholder="Enter the email" value="<%= user.getUserEmail()%>">
    </div>
    <div class="form-group">
        <label for="userpassword">Password</label>
        <input class="form-control userpassword" id="userpassword" name="userpassword" placeholder="Enter the password" type="password">
    </div>
    <div class="form-group">
        <label for="fktype">Type</label>
         <select class="form-control fktype" name="fktype">
            <%
                ModelType t = new ModelType();
                List<ModelType> types = t.listActive();
                if(types!=null){
                    for (ModelType type : types) {
            %>
                <option value="<%= type.getTypeId()%>" <% if(new String(type.getTypeId()).equals(user.getType().getTypeId())){ %> selected="selected" <% } %> > <%= type.getTypeDesignation() %></option>
            <%
                    }  
                }
            %>
          </select>
    </div>
    <label for="usersfile">Profile Image</label>
    <div class="input-group mb-3" id="articlesubtitle">
        <div class="custom-file">
          <input type="file" name="file" class="custom-file-input file" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
          <label class="custom-file-label" for="inputGroupFile01">Choose file to be the Profile Picture of the Profile</label>
        </div>
     </div>
    <div class="row">
        <div class="col">
            <button type="button" class="btn btn-danger btn-lg" onclick="event.preventDefault(); deleteUser(new FormData($('.updateUser')[0]))" style="width:100%;">Delete</button>
        </div>
        <div class="col">
            <button type="submit" class="btn btn-success btn-lg" style="width:100%;">Edit</button>
        </div>
    </div>
</form>
