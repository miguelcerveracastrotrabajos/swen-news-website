<%@page import="java.util.List"%>
<%@page import="Models.ModelLog"%>
<%@page import="Models.ModelUser"%>
<%@page import="Models.ModelArticle"%>
<%@page import="Models.ModelMedia"%>
<%@page import="Models.ModelType"%>
<%@page import="java.util.List"%>
<%@page import="Models.ModelArticle"%>
<%@page import="Models.ModelUser"%>
<%@page import="Models.ModelMedia"%>
<h1>List logs</h1>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-action-tab" data-toggle="pill" href="#pills-action" role="tab" aria-controls="pills-action" aria-selected="true">Actions</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-view-tab" data-toggle="pill" href="#pills-view" role="tab" aria-controls="pills-view" aria-selected="false">Views</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-error-tab" data-toggle="pill" href="#pills-error" role="tab" aria-controls="pills-error" aria-selected="false">Errors</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active table-responsive" id="pills-action" role="tabpanel" aria-labelledby="pills-action-tab">
        <table class="table">
             <thead>
               <tr>
                 <th scope="col">Timestamp</th>
                 <th scope="col">Owner</th>
                 <th scope="col">Description</th>
                 <th scope="col">Element</th>
                 
               </tr>
             </thead>
             <tbody >
                    <% ModelLog l = new ModelLog();
                    List<ModelLog> lstAction = l.listActionLogs();
                    if(lstAction!=null) {
                            for (ModelLog log : lstAction) {
                    %>
                    <tr>
                          <td class="timestampFont"><%= log.getCreated_at()%></td>
                          <td><img src="img/<%=log.getLogUser().getMedia().getMediaId()+"."+log.getLogUser().getMedia().getMediaType() %>" class="userMiniImage" style="margin-right:10px;"> <%= log.getLogUser().getUserName()  %></td>
                          <td style="vertical-align: middle;"><%= log.getLogDescription()%></td>
                          <td>
                            <%
                              if(new String(log.getLogDesignation()).equals("medias") && log.getLogElement()!=null){
                                      ModelMedia media = new ModelMedia();
                                      media = media.findById(log.getLogElement());
                                      if(new String(media.getMediaType()).equals("mp4")){ %> 
                                            <video width="150" height="84.313">
                                                <source src="img/<%=media.getMediaId()+"."+media.getMediaType() %>" type="video/<%= media.getMediaType() %>">
                                             </video>
                                        <% } else {%>
                                            <img src="img/<%=media.getMediaId()+"."+media.getMediaType() %>" class="imageMiniArticle">
                                        <% } 
                               }
                              if(new String(log.getLogDesignation()).equals("articles") && log.getLogElement()!=null){
                                      ModelArticle article = new ModelArticle();
                                      article = article.findById(log.getLogElement());
                                      %>
                                            <%= article.getArticleTitle() %>
                                      <%
                              }
                              if(new String(log.getLogDesignation()).equals("types") && log.getLogElement()!=null){
                                      ModelType type = new ModelType();
                                      type = type.findById(log.getLogElement());
                                      %>
                                            <%= type.getTypeDesignation()%>
                                      <%
                              }
                              if(new String(log.getLogDesignation()).equals("users") && log.getLogElement()!=null){
                                      ModelUser user = new ModelUser();
                                      user = user.findById(log.getLogElement());
                                      %>
                                        <img src="img/<%= user.getMedia().getMediaId()+"."+user.getMedia().getMediaType() %>" class="userMiniImage" style="margin-right:10px;">
                                      <%
                              }
                             %>
                          </td>
                          
                    </tr>
                    <%
                            }
                    }
                    %>
             </tbody>
        </table>
    </div>
    <div class="tab-pane fade table-responsive" id="pills-view" role="tabpanel" aria-labelledby="pills-view-tab">
        <table class="table">
             <thead>
               <tr>
                 <th scope="col" style="width:200px;">Timestamp</th>
                 <th scope="col" style="width:250px;">Owner</th>
                 <th scope="col">Description</th>
               </tr>
             </thead>
             <tbody>
               <%
                    List<ModelLog> lstView = l.listViewLogs();
                    if(lstView!=null) {
                            for (ModelLog log : lstView) {
                    %>
                    <tr>
                          <td class="timestampFont"><%= log.getCreated_at()%></td>
                          <td><img src="img/<%=log.getLogUser().getMedia().getMediaId()+"."+log.getLogUser().getMedia().getMediaType() %>" class="userMiniImage" style="margin-right:10px;"> <%= log.getLogUser().getUserName()  %></td>
                          <td style="vertical-align: middle;"><%= log.getLogDescription()%></td>
                    </tr>
                    <%
                            }
                    }
                    %>
             </tbody>
        </table>
    </div>
    <div class="tab-pane fade table-responsive" id="pills-error" role="tabpanel" aria-labelledby="pills-error-tab">
        <table class="table">
             <thead>
               <tr>
                 <th scope="col-3" style="width:200px;">Timestamp</th>
                 <th scope="col-3" style="width:220px;">Owner</th>
                 <th scope="col-6">Description</th>
                 
               </tr>
             </thead>
             <tbody >
                    <%
                    List<ModelLog> lstError= l.listErrorLogs();
                    if(lstError!=null) {
                        for (ModelLog log : lstError) {
                    %>
                    <tr>
                          <td class="timestampFont" style="width:175px;"><%= log.getCreated_at()%></td>
                          <td style="width:20px"><img src="img/<%=log.getLogUser().getMedia().getMediaId()+"."+log.getLogUser().getMedia().getMediaType() %>" class="userMiniImage" style="margin-right:10px;"> <%= log.getLogUser().getUserName()  %></td>
                          <td ><%= log.getLogDescription()%></td>
                    </tr>
                    <%
                            }
                    }
                    %>
             </tbody>
        </table>
    </div>
</div>
