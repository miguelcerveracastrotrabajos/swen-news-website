
<form class="createType" enctype="multipart/form-data" onsubmit="event.preventDefault(); insertTypeUser(new FormData($(this)[0]))">
    <h1>Create an User type</h1>
    <div class="form-group">
        <label for="typedesignation">Type Designation</label>
        <input class="form-control typedesignation" id="typedesignation" name="typedesignation" placeholder="Enter the Type Designation">
    </div>
    <div class="form-group">
        <label for="typedescription">Type Description</label>
        <input class="form-control articlesubtitle" id="typedescription" name="typedescription" placeholder="Enter the Type Description">
    </div>
    <div class="row">
        <div class="col">
            <a href="/ProjetoArtigos/types" class="btn btn-warning btn-lg" style="width:100%;">Cancel</a>
        </div>
        <div class="col">
            <button type="submit" class="btn btn-success btn-lg" style="width:100%;">Create</button>
        </div>
    </div>
</form>