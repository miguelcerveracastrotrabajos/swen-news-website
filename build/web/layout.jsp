<%@page import="Models.ModelUser"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="icon" href="/ProjetoArtigos/img/icon_bo.png">
         <!-- BOOTSTRAP AND SOME FONTS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
        <!-- NEW FONT -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <title>SNEW BackOffice</title>
    </head>
    <body>
        <% ModelUser auth = (ModelUser)request.getAttribute("auth"); %>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#"><img src="/ProjetoArtigos/img/icon_bo_extended.png" style="height:50px;"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
              <ul class="navbar-nav mr-auto">
                <% if(auth==null){%>
                <li class="nav-item active">
                  <a class="nav-link" href="/ProjetoArtigos/">Homepage<span class="sr-only">(current)</span></a>
                </li>
                <% } else {%>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Articles
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="/ProjetoArtigos/articles_create">Create Articles</a>
                      <a class="dropdown-item" href="/ProjetoArtigos/articles">All Articles</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      User Types
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="/ProjetoArtigos/types_create">Create User Type</a>
                      <a class="dropdown-item" href="/ProjetoArtigos/types">All User Types</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      User
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="/ProjetoArtigos/users_create">Create User</a>
                      <a class="dropdown-item" href="/ProjetoArtigos/users">All Users</a>
                    </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/ProjetoArtigos/logs">Logs</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/ProjetoArtigos/dashboard">Dashboard</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/ProjetoArtigos/users_authenticate"><i class="fas fa-sign-out-alt"></i></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/ProjetoArtigos/docs"><i class="fas fa-question-circle"></i></a>
                </li>
                <% } %>
                
                
                
               
              </ul>
              <% if(auth!=null){%>
              <span id="autenticateSquare" class="navbar-text">
                <img src="img/<%=auth.getMedia().getMediaId()+"."+auth.getMedia().getMediaType() %>"> Bem vindo <%= auth.getUserName() %>
              </span>
              <% } else {%>
              <span id="autenticateSquare" class="navbar-text">
                  <a class="nav-link" href="https://vincentgarreau.com/particles.js/"><img src="https://png.pngtree.com/svg/20170610/13bb84f49d.svg" style="height:25px;"> ParticleJs Library</a>
              </span>
              <% } %>
            </div>
        </nav>
        <div   <% if(auth==null){%> id="particles-js" <% }%>>
            <div  class="container" style="padding-top:15px;">
                <jsp:include page="views/${contentPage}.jsp" />
            </div>
            
        </div>
        <div <% if(auth!=null){%> id="particles-js" style="display:none;" <% }%>></div>
    </body>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script>
        particlesJS.load('particles-js', '/ProjetoArtigos/JSON/config.json', function() {
            console.log('Particles.json loaded');
        });
    </script>
</html>
