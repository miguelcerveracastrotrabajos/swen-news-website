//---------------------------------------------------MODULES AND INITIALIZER---------------------------
//Node modules to load
const express = require('express'); //Express Framework
const router = express.Router();    //Express Router
const request = require('request-promise');
const cheerio = require('cheerio');
//NEWS URLS
const pplwareNewsUrl = 'https://pplware.sapo.pt/';
const bbcNewsUrl='https://www.bbc.com/news';
const nytNewsUrl='https://www.nytimes.com/';
const elmundoUrl='https://www.elmundo.es';
//HELPERS
function removeDuplicates(d) {
    var newA=[];
    var newD=[];
    for (i = 0; i < d.length; i++) {
        if(!(newA.indexOf(d[i][0]) > -1)){
            newA.push(d[i][0]);
            newD.push(d[i]);
        }
    }
    return newD;
}
//EXTRACT NEWS FROM PEOPLEWARE (titles and images)
async function getPplwareNewsFromHtml(html){
    //ARRAY DOS URLS TITULOS
    var newsLink = [];
    //MAIN PAGE NEWS
    var $ = cheerio.load(html);
    $('.post').each(function(){
        var title=$(this).find('.post-title');
        var img=title.parent().find('img:first-child');
        newsLink.push([title.find('a').html(), title.find('a').attr('href'), img.attr('src')]);
    });
    newsLink=removeDuplicates(newsLink);
    return newsLink;
}
//EXTRACT NEWS FROM BBC (titles and images)
async function getBBCNewsFromHtml(html){
    //ARRAY DOS URLS TITULOS
    var newsLink = [];
    //MAIN PAGE NEWS
    var $ = cheerio.load(html);
    var index=0;
    $('.gel-layout__item').each(function(){
        index++;
        var url= $(this).find('.gs-c-promo-image').find('img').attr('data-src');
        if(url){
            url=url.replace("{width}", "210");
            newsLink.push([$(this).find('.gs-c-promo-heading>h3').html(), bbcNewsUrl+$(this).find('.gs-c-promo-heading').attr('href'), url]);
        }
        
    });
    
    newsLink=removeDuplicates(newsLink);
    return newsLink;
}
//EXTRACT NEWS FROM NEW YORK TIMES (just Titles)
async function getNYTNewsFromHtml(html){
    //ARRAY DOS URLS TITULOS
    var newsLink = [];
    //MAIN PAGE NEWS
    var $ = cheerio.load(html);
    var index=0;
    $('article.css-8atqhb').each(function(){
        newsLink.push([$(this).find('h2').html(), nytNewsUrl+$(this).find('a').attr('href'), $(this).find('img:first-child').attr('src')]);
    });
    
    newsLink=removeDuplicates(newsLink);
    return newsLink;
}
//EXTRACT NEWS FROM EL MUNDO
async function getElMundoNewsFromHtml(html){
    //ARRAY DOS URLS TITULOS
    var newsLink = [];
    //MAIN PAGE NEWS
    var $ = cheerio.load(html);
    var index=0;
    $('article.css-8atqhb').each(function(){
        newsLink.push([$(this).find('.ue-c-cover-content__headline').html(), $(this).find('.ue-c-cover-content__link').attr('href'), $(this).find('.ue-c-cover-content__figure>img').attr('src')]);
    });
    
    newsLink=removeDuplicates(newsLink);
    return newsLink;
}

//---------------------------------------------------ROUTES---------------------------------------------
//Get all articles (Route:/api/articles/)
router.get('/', async (req, res) => {
    const news = await request(bbcNewsUrl).then(response => getBBCNewsFromHtml(response)).catch(err => console.log(err));
    const news2 = await request(pplwareNewsUrl).then(response => getPplwareNewsFromHtml(response)).catch(err => console.log(err));
    const news3 = await request(nytNewsUrl).then(response => getNYTNewsFromHtml(response)).catch(err => console.log(err));
    //const news4 = await request(elmundoUrl).then(response => getElMundoNewsFromHtml(response)).catch(err => console.log(err));
    res.send([['bbc', news], ['pplware', news2], ['other',news3]]);
});

module.exports = router;


