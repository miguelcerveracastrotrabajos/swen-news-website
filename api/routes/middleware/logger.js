//Node modules to load
const moment = require('moment')    //Moment Module
//Middleware
const logger = (req, res, next) => {
    console.log("----------------------------Accessing--------------------------------");
    console.log(`Route: ${req.protocol}://${req.get('host')}${req.originalUrl}`);
    console.log(`Date: ${moment().format()}`);
    console.log("---------------------------------------------------------------------");
    next();
}

//Export
module.exports = logger;