//---------------------------------------------------MODULES AND INITIALIZER---------------------------
//Node modules to load
const express = require('express');                     //Express Framework
const path = require('path');                           //Path Module
const bodyParser = require('body-parser');              //Body Parser Module
const cors = require('cors');

//Call the middleware to show the route added and the time which the route is added
const logger = require('./routes/middleware/logger');

//Initialize Express 
const app = express();              

//---------------------------------------------------MIDDLEWARE-----------------------------------------
//Init middleware
app.use(logger);

//To Json
app.use(express.json());

//Form Submissions
app.use(express.urlencoded({extended:false}));

//Body parser
app.use(bodyParser.json());

//Cors
app.use(cors());

//------------------------------------------------ROUTES-------------------------------------------------

//Require ArticleRouter
const newsRouter = require('./routes/api/newsrouter.js');
//Call the Articlesrouter
app.use('/api/news', newsRouter);

//Set a static folder just for testing purposes
app.use(express.static(path.join(__dirname, 'public')));

app.use(express.urlencoded({extended:false}));

//------------------------------------------------PORT LISTENER--------------------------------------------
//Port variable
const PORT = process.env.PORT || 5000;

//Listen to port requests
app.listen(PORT, () =>{
    console.log("----------------------------SERVER-INFO------------------------------");
    console.log(`TITLE: NEWS EXTRACTION`);
    console.log(`AUTHOR: MIGUEL CERVERA CASTRO`);
    console.log(`PORT: ${PORT}`);
    console.log("---------------------------------------------------------------------");
});

